/**********************************************************************
 * Copyright (c) 2017, SaitamaSoft, All rights reserved. 
 ********************************************************************** 
 * 
 * Project Name : MvcMusicStore_SpringBoot
 * Date 		: Nov 22, 2017, 10:21:24 AM
 * Ver			: 1.0
 */
package com.harveynash.mvcmusicstorespringboot.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.harveynash.mvcmusicstorespringboot.model.Album;
import com.harveynash.mvcmusicstorespringboot.model.Genre;
import com.harveynash.mvcmusicstorespringboot.service.AlbumService;
import com.harveynash.mvcmusicstorespringboot.service.GenreService;

/**
 * @filename: HomeController.java
 * @author : Saitama Kenshin
 * @define : Controller for home page
 * 
 */
@Controller
public class HomeController {

	// Declare a field for dependency injection type GenreService
	@Autowired
	private GenreService genreService;

	// Declare a field for dependency injection type AlbumService
	@Autowired
	private AlbumService albumService;

	// Get request mapping with url pattern '/' and '/home'
	@RequestMapping(value = { "/", "/home" }, method = RequestMethod.GET)
	public String home(Model model) {

		// Get list of genre from findAllGenre method
		List<Genre> genre = genreService.findAllGenre();

		// Get list of album from getNewlyAddedAlbum method
		List<Album> album = albumService.getNewlyAddedAlbum();

		// Set list genre to attribute
		model.addAttribute("GenreList", genre);

		// Set list album to attribute
		model.addAttribute("AlbumList", album);

		// return mapping to home page
		return "home";
	}
}
