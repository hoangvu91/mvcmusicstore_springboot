/**********************************************************************
 * Copyright (c) 2017, SaitamaSoft, All rights reserved. 
 ********************************************************************** 
 * 
 * Project Name : MvcMusicStore_SpringBoot
 * Date 		: Nov 22, 2017, 10:38:21 AM
 * Ver			: 1.0
 */
package com.harveynash.mvcmusicstorespringboot.controller;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.harveynash.mvcmusicstorespringboot.model.Album;
import com.harveynash.mvcmusicstorespringboot.model.Genre;
import com.harveynash.mvcmusicstorespringboot.service.AlbumService;
import com.harveynash.mvcmusicstorespringboot.service.GenreService;

/**
 * @filename: GenreController.java
 * @author	: Saitama Kenshin
 * @define	: Controller for all genre request
 * 
 */
@Controller
public class GenreController {

	// Declare a field for dependency injection type GenreService
	@Autowired
	private GenreService genreService;

	// Declare a field for dependency injection type AlbumService
	@Autowired
	private AlbumService albumService;

	// Get request mapping with url pattern '/' and '/home'
	@RequestMapping(value = { "/Genre" }, method = RequestMethod.GET)
	public String home(Model model, HttpServletRequest request) {

		// Get list of genre from findAllGenre method
		List<Genre> genreList = genreService.findAllGenre();

		// Get list of album from getNewlyAddedAlbum method
		Genre genre = genreService.findByGenreId(Integer.parseInt(request.getParameter("id")));

		// Get list of album from getNewlyAddedAlbum method
		List<Album> album = albumService.getAlbumByGenreId(Integer.parseInt(request.getParameter("id")));

		// Set list genre to attribute
		model.addAttribute("GenreList", genreList);

		// Set genre to attribute
		model.addAttribute("Genre", genre);

		// Set list album to attribute
		model.addAttribute("AlbumList", album);

		// return mapping to home page
		return "albumByGenre";
	}
}

