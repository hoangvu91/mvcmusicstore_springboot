/**********************************************************************
 * Copyright (c) 2017, SaitamaSoft, All rights reserved. 
 ********************************************************************** 
 * 
 * Project Name : MvcMusicStore_SpringBoot
 * Date 		: Nov 17, 2017, 12:55:33 AM
 * Ver			: 1.0
 */
package com.harveynash.mvcmusicstorespringboot.controller;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.harveynash.mvcmusicstorespringboot.model.User;
import com.harveynash.mvcmusicstorespringboot.service.UserService;

/**
 * @filename: UserController.java
 * @author : Saitama Kenshin
 * @define : Controller for user
 * 
 */
@Controller
public class UserController {

	// Declare a field for dependency injection type AlbumService
	@Autowired
	private UserService userService;
	
	@Autowired
	private PasswordEncoder passwordEncoder;
	
	// Get request mapping with url pattern '/register'
	@RequestMapping(value = "/register", method = RequestMethod.GET)
	public String register(Model model) {

		// return mapping to register page
		return "register";
	}

	// Get request mapping with url pattern '/register'
	@RequestMapping(value = "/register", method = RequestMethod.POST)
	public String saveUser(Model model, HttpServletRequest request) {
		
		User user = new User();	
		
		user.setUsername(request.getParameter("txtUserName"));
		user.setPassword(passwordEncoder.encode(request.getParameter("txtPassword")));
		user.setFirstName(request.getParameter("txtFirstName"));
		user.setLastName(request.getParameter("txtLastName"));
		user.setAddress(request.getParameter("txtAddress"));
		user.setCity(request.getParameter("txtCity"));
		user.setState(request.getParameter("txtState"));
		user.setPostalCode(request.getParameter("txtPostalCode"));
		user.setCountry(request.getParameter("txtCountry"));
		user.setPhone(request.getParameter("txtPhone"));
		user.setEmail(request.getParameter("txtEmail"));
				
		userService.save(user);
		// return mapping to register page
		return "login";
	}
}
