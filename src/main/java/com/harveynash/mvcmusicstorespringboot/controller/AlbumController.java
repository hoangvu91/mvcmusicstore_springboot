/**********************************************************************
 * Copyright (c) 2017, SaitamaSoft, All rights reserved. 
 ********************************************************************** 
 * 
 * Project Name : MvcMusicStore_SpringBoot
 * Date 		: Nov 14, 2017, 5:40:01 PM
 * Ver			: 1.0
 */
package com.harveynash.mvcmusicstorespringboot.controller;

import java.io.IOException;
import java.math.BigDecimal;
import java.sql.Timestamp;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

import com.harveynash.mvcmusicstorespringboot.model.Album;
import com.harveynash.mvcmusicstorespringboot.model.Artist;
import com.harveynash.mvcmusicstorespringboot.model.FileBucket;
import com.harveynash.mvcmusicstorespringboot.model.Genre;
import com.harveynash.mvcmusicstorespringboot.model.Order;
import com.harveynash.mvcmusicstorespringboot.model.OrderDetail;
import com.harveynash.mvcmusicstorespringboot.service.AlbumService;
import com.harveynash.mvcmusicstorespringboot.service.ArtistService;
import com.harveynash.mvcmusicstorespringboot.service.GenreService;
import com.harveynash.mvcmusicstorespringboot.service.OrderDetailService;
import com.harveynash.mvcmusicstorespringboot.service.OrderService;
import com.harveynash.mvcmusicstorespringboot.service.UserService;
import com.harveynash.mvcmusicstorespringboot.util.FileCreator;
import com.harveynash.mvcmusicstorespringboot.util.FileValidator;

/**
 * @filename: AlbumController.java
 * @author : Saitama Kenshin
 * @define : Controller for all album request
 * 
 */
@Controller
public class AlbumController {

	// Declare a field for dependency injection type AlbumService
	@Autowired
	private AlbumService albumService;

	// Declare a field for dependency injection type ArtistService
	@Autowired
	private ArtistService artistService;

	// Declare a field for dependency injection type GenreService
	@Autowired
	private GenreService genreService;

	// Declare a field for dependency injection type UserService
	@Autowired
	private UserService userService;

	// Declare a field for dependency injection type OrderService
	@Autowired
	private OrderService orderService;

	// Declare a field for dependency injection type OrderService
	@Autowired
	private OrderDetailService orderDetailService;

	// Declare a field for dependency injection type MessageSource
	@Autowired
	MessageSource messageSource;

	// Declare a field for dependency injection type FileValidator
	@Autowired
	FileValidator fileValidator;

	@Autowired
	FileCreator fileCreator;

	@InitBinder("fileBucket")
	protected void initBinder(WebDataBinder binder) {
		binder.setValidator(fileValidator);
	}

	// Get request mapping with url pattern '/albumDetail'
	@RequestMapping(value = { "/albumDetail" }, method = RequestMethod.GET)
	public String albumDetail(Model model, HttpServletRequest request) {

		Album album = albumService.findByAlbumId(Integer.parseInt(request.getParameter("id")));

		// Set list album to attribute
		model.addAttribute("Album", album);
		// return mapping to manage album page
		return "albumDetail";

	}

	// Get request mapping with url pattern /images/ and get image from tomcat
	// server
	@RequestMapping(value = "/images/{imageName}.{ext}")
	@ResponseBody
	public byte[] getImage(@PathVariable(value = "imageName") String imageName, @PathVariable("ext") String ext) throws IOException {
				
		return fileCreator.getImages(imageName, ext);
	}

	// Get request mapping with url pattern '/admin/manageAlbum'
	@RequestMapping(value = { "/admin/manageAlbum" }, method = RequestMethod.GET)
	public String manageAlbum(Model model) {

		// Get list of album from getNewlyAddedAlbum method
		List<Album> album = albumService.getAllAlbum();

		// Set list album to attribute
		model.addAttribute("AlbumList", album);
		// return mapping to manage album page
		return "manageAlbum";
	}

	// Get request mapping with url pattern '/admin/add'
	@RequestMapping(value = { "/admin/add" }, method = RequestMethod.GET)
	public String addNew(Model model) {

		// Get list of genre from findAllGenre method
		List<Genre> genre = genreService.findAllGenre();

		// Get list of album from getNewlyAddedAlbum method
		List<Artist> artist = artistService.getAllArtist();

		// Set list album to attribute
		model.addAttribute("ArtistList", artist);

		// Set list genre to attribute
		model.addAttribute("GenreList", genre);

		// return mapping to home page
		return "addAlbum";
	}

	// Get request mapping with url pattern '/admin/manageAlbum'
	@RequestMapping(value = { "/admin/saveAlbum" }, method = RequestMethod.POST)
	public String saveAlbum(@Valid FileBucket fileBucket, BindingResult result, ModelMap model,
			HttpServletRequest request) throws IOException {

		if (result.hasErrors()) {
			System.out.println("Loi upload file");

			// return mapping to home page
			return "addAlbum";
		}

		else {
			Album album = new Album();

			MultipartFile multipartFile = fileBucket.getFile();

			album.setTitle(request.getParameter("txtAlbumTitle"));
			album.setGenreId(Integer.parseInt(request.getParameter("selectGenre")));
			album.setArtistId(Integer.parseInt(request.getParameter("selectArtist")));
			album.setPrice(new BigDecimal(request.getParameter("txtAlbumPrice")));
			album.setAlbumArtUrl(multipartFile.getOriginalFilename());

			albumService.save(album);

			fileCreator.createImage(multipartFile.getOriginalFilename(), multipartFile);

			// Get list of album from getNewlyAddedAlbum method
			List<Album> listAlbum = albumService.getAllAlbum();

			// Set list album to attribute
			model.addAttribute("AlbumList", listAlbum);

			// return mapping to home page
			return "manageAlbum";
		}

	}

	// Get request mapping with url pattern '/admin/manageAlbum'
	@RequestMapping(value = { "/admin/edit" }, method = RequestMethod.GET)
	public String edit(Model model, HttpServletRequest request) {

		model.addAttribute("genres_list", genreService.findAllGenre());
		model.addAttribute("artists_list", artistService.getAllArtist());
		model.addAttribute("album", albumService.findByAlbumId(Integer.parseInt(request.getParameter("id"))));
		return "editAlbum";

	}

	// Get request mapping with url pattern '/admin/manageAlbum'
	@RequestMapping(value = { "/admin/updateAlbum" }, method = RequestMethod.POST)
	public String updateAlbum(@ModelAttribute("album") Album album, FileBucket fileBucket, BindingResult result,
			ModelMap model, HttpServletRequest request) throws IOException {

		MultipartFile multipartFile = null;

		if (!fileBucket.getFile().isEmpty()) {
			multipartFile = fileBucket.getFile();
			album.setAlbumArtUrl(multipartFile.getOriginalFilename());
			fileCreator.createImage(multipartFile.getOriginalFilename(), multipartFile);
		}

		albumService.update(album);

		// Get list of album from getNewlyAddedAlbum method
		List<Album> listAlbum = albumService.getAllAlbum();

		// Set list album to attribute
		model.addAttribute("AlbumList", listAlbum);

		model.addAttribute("genres_list", genreService.findAllGenre());
		model.addAttribute("artists_list", artistService.getAllArtist());
		model.addAttribute("album", album);
		// return mapping to home page
		return "manageAlbum";
	}

	// Get request mapping with url pattern '/admin/manageAlbum'
	@RequestMapping(value = { "/admin/delete" }, method = RequestMethod.GET)
	public String delete(Model model, HttpServletRequest request) {

		albumService.delete(Integer.parseInt(request.getParameter("id")));

		// Get list of album from getNewlyAddedAlbum method
		List<Album> album = albumService.getAllAlbum();

		// Set list album to attribute
		model.addAttribute("AlbumList", album);
		// return mapping to manage album page
		return "manageAlbum";

	}

	// Get request mapping with url pattern '/admin/addToCart'
	@RequestMapping(value = { "/shoppingCart" }, method = RequestMethod.GET)
	public String shoppingCart(Model model, HttpServletRequest request) {
		return "shoppingCart";
	}

	// Get request mapping with url pattern '/admin/addToCart'
	@SuppressWarnings("unchecked")
	@RequestMapping(value = { "/addToCart" }, method = RequestMethod.POST)
	public String addToCart(Model model, HttpServletRequest request) {
		HttpSession session = request.getSession();
		HashMap<Album, Integer> listCart = (HashMap<Album, Integer>) session.getAttribute("LISTCART");

		if (listCart == null) {
			listCart = new HashMap<Album, Integer>();
		}

		int id = Integer.parseInt(request.getParameter("txtAlbumId"));
		Album album = albumService.findByAlbumId(id);

		int storeQuantity = 1;

		if (listCart.containsKey(album)) {
			storeQuantity = listCart.get(album) + 1;
		}

		listCart.put(album, storeQuantity);
		session.setAttribute("LISTCART", listCart);
		return "redirect:shoppingCart";

	}

	// Get request mapping with url pattern '/admin/addToCart'
	@SuppressWarnings("unchecked")
	@RequestMapping(value = { "/editCart" }, method = RequestMethod.POST)
	public String editCart(Model model, HttpServletRequest request) {

		HttpSession session = request.getSession();
		HashMap<Album, Integer> listCart = (HashMap<Album, Integer>) session.getAttribute("LISTCART");

		if (listCart == null) {
			listCart = new HashMap<Album, Integer>();
		}

		int id = Integer.parseInt(request.getParameter("txtAlbumId"));
		Album album = albumService.findByAlbumId(id);

		int quantity = Integer.parseInt(request.getParameter("txtQuantity"));

		listCart.put(album, quantity);

		session.setAttribute("LISTCART", listCart);
		return "redirect:shoppingCart";

	}

	// Get request mapping with url pattern '/admin/addToCart'
	@SuppressWarnings("unchecked")
	@RequestMapping(value = { "/removeCart" }, method = RequestMethod.GET)
	public String removeCart(Model model, HttpServletRequest request) {

		HttpSession session = request.getSession(false);

		if (session != null) {

			HashMap<Album, Integer> listCart = (HashMap<Album, Integer>) session.getAttribute("LISTCART");

			if (listCart != null) {
				int id = Integer.parseInt(request.getParameter("id"));
				Album album = albumService.findByAlbumId(id);

				if (listCart.containsKey(album)) {
					listCart.remove(album);
				}

				session.setAttribute("CART", listCart);
			}
		}

		return "shoppingCart";
	}

	// Get request mapping with url pattern '/checkout'
	@RequestMapping(value = { "/checkout" }, method = RequestMethod.GET)
	public String checkout(Model model, HttpServletRequest request) {

		model.addAttribute("total", request.getParameter("total"));

		return "checkout";
	}

	@SuppressWarnings("unchecked")
	@RequestMapping(value = { "/checkout" }, method = RequestMethod.POST)
	public String saveCheckOut(Model model, HttpServletRequest request) {

		HttpSession session = request.getSession();
		HashMap<Album, Integer> listCart = (HashMap<Album, Integer>) session.getAttribute("LISTCART");

		Order order = new Order();

		order.setOrderDate(new Timestamp(Calendar.getInstance().getTimeInMillis()));

		order.setUserId(userService.findByUserName(request.getUserPrincipal().getName()).getUserId());

		if (request.getParameter("txtAddress").isEmpty()) {
			order.setAddress(userService.findByUserName(request.getUserPrincipal().getName()).getAddress());
		} else {
			order.setAddress(request.getParameter("txtAddress"));
		}

		if (request.getParameter("txtPhone").isEmpty()) {
			order.setPhone(userService.findByUserName(request.getUserPrincipal().getName()).getPhone());
		} else {
			order.setPhone(request.getParameter("txtPhone"));
		}

		order.setTotal(new BigDecimal(request.getParameter("txtTotal")));

		orderService.save(order);

		double sub = 0.0d;

		for (HashMap.Entry<Album, Integer> entry : listCart.entrySet()) {
			System.out.println(entry.getKey() + "/" + entry.getValue());
			sub = entry.getKey().getPrice().doubleValue() * ((double) entry.getValue());

			OrderDetail orderDetail = new OrderDetail();
			orderDetail.setOrderId(order.getOrderId());
			orderDetail.setAlbumId(entry.getKey().getAlbumId());
			orderDetail.setQuantity(entry.getValue());
			orderDetail.setUnitPrice(new BigDecimal(sub));

			orderDetailService.save(orderDetail);
		}

		listCart.clear();
		return "checkoutSuccess";
	}
}
