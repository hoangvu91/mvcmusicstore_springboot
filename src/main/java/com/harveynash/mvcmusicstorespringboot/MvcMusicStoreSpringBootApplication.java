package com.harveynash.mvcmusicstorespringboot;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MvcMusicStoreSpringBootApplication {

	public static void main(String[] args) {
		SpringApplication.run(MvcMusicStoreSpringBootApplication.class, args);
	}
}
