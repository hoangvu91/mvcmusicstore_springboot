/**********************************************************************
 * Copyright (c) 2017, SaitamaSoft, All rights reserved. 
 ********************************************************************** 
 * 
 * Project Name : MvcMusicStore_SpringBoot
 * Date 		: Nov 12, 2017, 5:05:33 PM
 * Ver			: 1.0
 */
package com.harveynash.mvcmusicstorespringboot.security;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;

/**
 * @filename: SecurityConfiguration.java
 * @author : Saitama Kenshin
 * @define : Create Spring Security Java Configuration. This configuration
 *         creates a Servlet Filter known as the springSecurityFilterChain which
 *         is responsible for all the security (protecting the application URLs,
 *         validating submitted username and passwords, redirecting to the log
 *         in form, etc) within our application.
 * 
 */
@Configuration
@EnableWebSecurity
public class SecurityConfiguration extends WebSecurityConfigurerAdapter {

	@Autowired
	@Qualifier("customUserDetailsService")
	UserDetailsService userDetailsService;

	@Autowired
	CustomSuccessHandler customSuccessHandler;

	@Bean
	public PasswordEncoder passwordEncoder() {
		return new BCryptPasswordEncoder();
	}

	/**
	 * Method configureGlobalSecurity configures AuthenticationManagerBuilder with
	 * user credentials and allowed roles.
	 * 
	 * This AuthenticationManagerBuilder creates AuthenticationManager which is
	 * responsible for processing any authentication request.
	 */
	@Autowired
	public void configureGlobalSecurity(AuthenticationManagerBuilder auth) throws Exception {
		auth.userDetailsService(userDetailsService).passwordEncoder(passwordEncoder());
	}

	/**
	 * Method Configure configures HttpSecurity which allows configuring web based
	 * security for specific http requests.
	 */

	@Override
	protected void configure(HttpSecurity http) throws Exception {

		// Default config
		http.authorizeRequests().antMatchers("/", "/home").permitAll().antMatchers("/user/**")
				.access("hasRole('USER') and hasRole('ADMIN')").antMatchers("/checkout").hasAnyRole("USER", "ADMIN")
				.antMatchers("/admin/**").access("hasRole('ADMIN')").and().formLogin().loginPage("/login")
				.usernameParameter("username").passwordParameter("password").and().csrf().disable().exceptionHandling()
				.accessDeniedPage("/Access_Denied");

	}
}
