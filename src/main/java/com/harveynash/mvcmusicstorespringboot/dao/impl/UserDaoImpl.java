/**********************************************************************
 * Copyright (c) 2017, SaitamaSoft, All rights reserved. 
 ********************************************************************** 
 * 
 * Project Name : MvcMusicStore_SpringBoot
 * Date 		: Nov 13, 2017, 4:46:09 PM
 * Ver			: 1.0
 */
package com.harveynash.mvcmusicstorespringboot.dao.impl;

import org.hibernate.Criteria;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;

import com.harveynash.mvcmusicstorespringboot.dao.AbstractDao;
import com.harveynash.mvcmusicstorespringboot.dao.UserDao;
import com.harveynash.mvcmusicstorespringboot.model.User;
 
@Repository("userDao")
public class UserDaoImpl extends AbstractDao<Integer, User> implements UserDao {

	// Method find user by id
	public User findByUserId(int id) {
		return getByKey(id);
	}

	// Method find user by username
	public User findByUserName(String username) {
		Criteria crit = createEntityCriteria();
        crit.add(Restrictions.eq("username", username));
        return (User) crit.uniqueResult();
	}

	// Method get newly add user
	public User getNewlyAddedUserId() {
		Criteria crit = createEntityCriteria();
		return (User) crit.setMaxResults(1).uniqueResult();
	}

	// Method create new user
	public void save(User user) {
		persist(user);
	}
}
