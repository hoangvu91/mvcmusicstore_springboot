/**********************************************************************
 * Copyright (c) 2017, SaitamaSoft, All rights reserved. 
 ********************************************************************** 
 * 
 * Project Name : MvcMusicStore_SpringBoot
 * Date 		: Nov 12, 2017, 3:51:52 PM
 * Ver			: 1.0
 */
package com.harveynash.mvcmusicstorespringboot.dao.impl;

import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;

import com.harveynash.mvcmusicstorespringboot.dao.AbstractDao;
import com.harveynash.mvcmusicstorespringboot.dao.AlbumDao;
import com.harveynash.mvcmusicstorespringboot.model.Album;

/**
 * @filename: AlbumDaoImpl.java
 * @author : Saitama Kenshin
 * @define : All methods implementation of AlbumDao
 * 
 */
@Repository("AlbumDao")
public class AlbumDaoImpl extends AbstractDao<Integer, Album> implements AlbumDao {

	// Method get album by key
	public Album findByAlbumId(int id) {
		return getByKey(id);
	}

	// Method get album by title
	@Override
	public Album findByAlbumTitle(String title) {
		Criteria crit = createEntityCriteria();
        crit.add(Restrictions.eq("title", title));
        Album album = (Album)crit.uniqueResult();
        return album;
	}
	
	// Method get 22 records newly added of album in db
	@SuppressWarnings("unchecked")
	public List<Album> getNewlyAddedAlbum() {
		Criteria crit = createEntityCriteria();

		crit.addOrder(Order.desc("albumId"));
		crit.setMaxResults(22);

		return (List<Album>) crit.list();
	}

	// Method get all album
	@SuppressWarnings("unchecked")
	public List<Album> getAllAlbum() {
		Criteria crit = createEntityCriteria();
		crit.addOrder(Order.desc("albumId"));
		return (List<Album>) crit.list();
	}

	// Method get album by genre id
	@SuppressWarnings("unchecked")
	public List<Album> getAlbumByGenreId(int id) {
		Criteria crit = createEntityCriteria();
		crit.addOrder(Order.desc("albumId"));
		crit.add(Restrictions.eq("genreId", id));
		return (List<Album>) crit.list();
	}
	
	// Method add new album
	public void save(Album album) {
		persist(album);
	}

	// Method delete an album by albumId
	public void delete(int id) {
		Criteria crit = createEntityCriteria();
		crit.add(Restrictions.eq("albumId", id));
		Album album = (Album) crit.uniqueResult();
		delete(album);
	}
}
