/**********************************************************************
 * Copyright (c) 2017, SaitamaSoft, All rights reserved. 
 ********************************************************************** 
 * 
 * Project Name : MvcMusicStore_SpringBoot
 * Date 		: Dec 1, 2017, 1:16:28 AM
 * Ver			: 1.0
 */
package com.harveynash.mvcmusicstorespringboot.dao.impl;

import org.springframework.stereotype.Repository;

import com.harveynash.mvcmusicstorespringboot.dao.AbstractDao;
import com.harveynash.mvcmusicstorespringboot.dao.OrderDetailDao;
import com.harveynash.mvcmusicstorespringboot.model.OrderDetail;

/**
 * @filename: OrderDetailDaoImpl.java
 * @author	: Saitama Kenshin
 * @define	: 
 * 
 */
@Repository("orderDetailDao")
public class OrderDetailDaoImpl extends AbstractDao<Integer, OrderDetail> implements OrderDetailDao{

	public void save(OrderDetail orderDetail) {
		persist(orderDetail);
	}
}
