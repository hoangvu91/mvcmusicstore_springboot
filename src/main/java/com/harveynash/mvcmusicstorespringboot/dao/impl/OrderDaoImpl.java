/**********************************************************************
 * Copyright (c) 2017, SaitamaSoft, All rights reserved. 
 ********************************************************************** 
 * 
 * Project Name : MvcMusicStore_SpringBoot
 * Date 		: Nov 29, 2017, 10:53:24 PM
 * Ver			: 1.0
 */
package com.harveynash.mvcmusicstorespringboot.dao.impl;

import org.springframework.stereotype.Repository;

import com.harveynash.mvcmusicstorespringboot.dao.AbstractDao;
import com.harveynash.mvcmusicstorespringboot.dao.OrderDao;
import com.harveynash.mvcmusicstorespringboot.model.Order;

/**
 * @filename: OrderDaoImpl.java
 * @author	: Saitama Kenshin
 * @define	: 
 * 
 */
@Repository("orderDao")
public class OrderDaoImpl extends AbstractDao<Integer, Order> implements OrderDao{

	// Method create new order
	public void save(Order order) {
		persist(order);
	}
}
