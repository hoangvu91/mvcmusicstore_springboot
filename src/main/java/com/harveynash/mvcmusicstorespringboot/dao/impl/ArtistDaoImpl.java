/**********************************************************************
 * Copyright (c) 2017, SaitamaSoft, All rights reserved. 
 ********************************************************************** 
 * 
 * Project Name : MvcMusicStore_SpringBoot
 * Date 		: Nov 15, 2017, 11:06:10 AM
 * Ver			: 1.0
 */
package com.harveynash.mvcmusicstorespringboot.dao.impl;

import java.util.List;

import org.hibernate.Criteria;
import org.springframework.stereotype.Repository;

import com.harveynash.mvcmusicstorespringboot.dao.AbstractDao;
import com.harveynash.mvcmusicstorespringboot.dao.ArtistDao;
import com.harveynash.mvcmusicstorespringboot.model.Artist;

/**
 * @filename: ArtistDaoImpl.java
 * @author : Saitama Kenshin
 * @define :
 * 
 */
@Repository("ArtistDao")
public class ArtistDaoImpl extends AbstractDao<Integer, Artist> implements ArtistDao {

	// Method get album by key
	public Artist findByArtistId(int id) {
		return getByKey(id);
	}

	// Method get all artist
	@SuppressWarnings("unchecked")
	public List<Artist> getAllArtist() {
		Criteria crit = createEntityCriteria();
		return (List<Artist>) crit.list();
	}
}
