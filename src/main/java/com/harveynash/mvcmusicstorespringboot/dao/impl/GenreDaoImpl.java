/**********************************************************************
 * Copyright (c) 2017, SaitamaSoft, All rights reserved. 
 ********************************************************************** 
 * 
 * Project Name : MvcMusicStore_SpringBoot
 * Date 		: Nov 22, 2017, 10:56:25 AM
 * Ver			: 1.0
 */
package com.harveynash.mvcmusicstorespringboot.dao.impl;

import java.util.List;

import org.hibernate.Criteria;
import org.springframework.stereotype.Repository;

import com.harveynash.mvcmusicstorespringboot.dao.AbstractDao;
import com.harveynash.mvcmusicstorespringboot.dao.GenreDao;
import com.harveynash.mvcmusicstorespringboot.model.Genre;
/**
 * @filename: GenreDaoImpl.java
 * @author	: Saitama Kenshin
 * @define	: 
 * 
 */
@Repository("GenreDao")
public class GenreDaoImpl extends AbstractDao<Integer, Genre> implements GenreDao {

	// Method get genre by key
	public Genre findByGenreId(int id) {
		return getByKey(id);
	}
	
	// Method get all genre in database
	@SuppressWarnings("unchecked")
	public List<Genre> findAllGenre() {
		Criteria crit = createEntityCriteria();
        return (List<Genre>) crit.list();
	}
}
