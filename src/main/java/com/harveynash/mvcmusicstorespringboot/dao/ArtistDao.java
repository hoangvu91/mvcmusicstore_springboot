/**********************************************************************
 * Copyright (c) 2017, SaitamaSoft, All rights reserved. 
 ********************************************************************** 
 * 
 * Project Name : MvcMusicStore_SpringBoot
 * Date 		: Nov 15, 2017, 11:03:25 AM
 * Ver			: 1.0
 */
package com.harveynash.mvcmusicstorespringboot.dao;

import java.util.List;

import com.harveynash.mvcmusicstorespringboot.model.Artist;

/**
 * @filename: ArtistDao.java
 * @author : Saitama Kenshin
 * @define :
 * 
 */
public interface ArtistDao {

	// Method get all album
	List<Artist> getAllArtist();
}
