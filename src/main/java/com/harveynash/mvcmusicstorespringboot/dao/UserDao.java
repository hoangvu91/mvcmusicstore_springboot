/**********************************************************************
 * Copyright (c) 2017, SaitamaSoft, All rights reserved. 
 ********************************************************************** 
 * 
 * Project Name : MvcMusicStore_SpringBoot
 * Date 		: Nov 13, 2017, 4:45:15 PM
 * Ver			: 1.0
 */
package com.harveynash.mvcmusicstorespringboot.dao;

import com.harveynash.mvcmusicstorespringboot.model.User;

/**
 * @filename: UserDao.java
 * @author	: Saitama Kenshin
 * @define	: 
 * 
 */
public interface UserDao {

	// Method find user by id
	User findByUserId(int id);
	
    // Method get id of newly added user
    User getNewlyAddedUserId();
    
	// Method find user by username
    User findByUserName(String username);
    
    // Method create new user
    void save(User user);
}
