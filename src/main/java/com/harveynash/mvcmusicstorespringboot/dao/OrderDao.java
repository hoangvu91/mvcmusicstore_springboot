/**********************************************************************
 * Copyright (c) 2017, SaitamaSoft, All rights reserved. 
 ********************************************************************** 
 * 
 * Project Name : MvcMusicStore_SpringBoot
 * Date 		: Nov 29, 2017, 10:51:53 PM
 * Ver			: 1.0
 */
package com.harveynash.mvcmusicstorespringboot.dao;

import com.harveynash.mvcmusicstorespringboot.model.Order;

/**
 * @filename: OrderDao.java
 * @author	: Saitama Kenshin
 * @define	: 
 * 
 */
public interface OrderDao {

    // Method create new order
    void save(Order order);
}
