/**********************************************************************
 * Copyright (c) 2017, SaitamaSoft, All rights reserved. 
 ********************************************************************** 
 * 
 * Project Name : MvcMusicStore_SpringBoot
 * Date 		: Nov 12, 2017, 3:50:34 PM
 * Ver			: 1.0
 */
package com.harveynash.mvcmusicstorespringboot.dao;

import java.util.List;

import com.harveynash.mvcmusicstorespringboot.model.Album;

/**
 * @filename: AlbumDao.java
 * @author : Saitama Kenshin
 * @define : All methods declaration of AlbumDao
 * 
 */
public interface AlbumDao {

	// Method get album by album id
	Album findByAlbumId(int id);

	// Method find album by title
	Album findByAlbumTitle(String title);
	
	// Method get newly added Album
	List<Album> getNewlyAddedAlbum();

	// Method get all album
	List<Album> getAllAlbum();
	
	// Method get album by genre id
	List<Album> getAlbumByGenreId(int id);

	// Method insert new album
	void save(Album album);

	// Method delete album
	void delete(int id);	
}
