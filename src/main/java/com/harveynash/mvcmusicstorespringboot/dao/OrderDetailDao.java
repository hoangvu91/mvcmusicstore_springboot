/**********************************************************************
 * Copyright (c) 2017, SaitamaSoft, All rights reserved. 
 ********************************************************************** 
 * 
 * Project Name : MvcMusicStore_SpringBoot
 * Date 		: Dec 1, 2017, 1:15:12 AM
 * Ver			: 1.0
 */
package com.harveynash.mvcmusicstorespringboot.dao;

import com.harveynash.mvcmusicstorespringboot.model.OrderDetail;

/**
 * @filename: OrderDetailDao.java
 * @author : Saitama Kenshin
 * @define :
 * 
 */
public interface OrderDetailDao {
	
	// Method create new order
	void save(OrderDetail orderDetail);
}
