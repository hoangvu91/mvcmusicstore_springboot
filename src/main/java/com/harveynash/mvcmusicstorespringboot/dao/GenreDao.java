/**********************************************************************
 * Copyright (c) 2017, SaitamaSoft, All rights reserved. 
 ********************************************************************** 
 * 
 * Project Name : MvcMusicStore_SpringBoot
 * Date 		: Nov 11, 2017, 11:00:16 PM
 * Ver			: 1.0
 */
package com.harveynash.mvcmusicstorespringboot.dao;

import java.util.List;

import com.harveynash.mvcmusicstorespringboot.model.Genre;

/**
 * @filename: GenreDao.java
 * @author : Saitama Kenshin
 * @define : All methods declaration of GenreDao
 * 
 */
public interface GenreDao {

	// Method get album by album id
	Genre findByGenreId(int id);

	// Method get list genre
	List<Genre> findAllGenre();
}
