/**********************************************************************
 * Copyright (c) 2017, SaitamaSoft, All rights reserved. 
 ********************************************************************** 
 * 
 * Project Name : MvcMusicStore_SpringBoot
 * Date 		: Nov 14, 2017, 5:40:01 PM
 * Ver			: 1.0
 */
package com.harveynash.mvcmusicstorespringboot.api;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.harveynash.mvcmusicstorespringboot.model.Artist;
import com.harveynash.mvcmusicstorespringboot.service.ArtistService;

/**
 * @filename: ArtistApi.java
 * @author : Saitama Kenshin
 * @define : Api for all artist request
 * 
 */
@RestController
@RequestMapping("/api")
public class ArtistApi {

	// Declare a field for dependency injection type ArtistService
	@Autowired
	private ArtistService artistService;

	// Get request mapping with url pattern '/' and '/home'
	@RequestMapping(value = { "/artist" }, method = RequestMethod.GET)
	public List<Artist> getAllArtist() {

		// Get list of genre from findAllGenre method
		List<Artist> artistList = artistService.getAllArtist();

		// return mapping to home page
		return artistList;
	}
}
