/**********************************************************************
 * Copyright (c) 2017, SaitamaSoft, All rights reserved. 
 ********************************************************************** 
 * 
 * Project Name : MvcMusicStore_SpringBoot
 * Date 		: Dec 14, 2017, 5:40:01 PM
 * Ver			: 1.0
 */
package com.harveynash.mvcmusicstorespringboot.api;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.harveynash.mvcmusicstorespringboot.model.Genre;
import com.harveynash.mvcmusicstorespringboot.service.GenreService;

/**
 * @filename: GenreApi.java
 * @author : Saitama Kenshin
 * @define : Api for all genre request
 * 
 */
@RestController
@RequestMapping("/api")
public class GenreApi {

	// Declare a field for dependency injection type GenreService
	@Autowired
	private GenreService genreService;

	// Get request mapping with url pattern '/' and '/home'
	@RequestMapping(value = { "/genre" }, method = RequestMethod.GET)
	public List<Genre> findAllGenre() {

		// Get list of genre from findAllGenre method
		List<Genre> genreList = genreService.findAllGenre();

		// return mapping to home page
		return genreList;
	}
}
