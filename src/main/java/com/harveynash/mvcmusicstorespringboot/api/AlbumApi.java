/**********************************************************************
 * Copyright (c) 2017, SaitamaSoft, All rights reserved. 
 ********************************************************************** 
 * 
 * Project Name : MvcMusicStore_SpringBoot
 * Date 		: Nov 14, 2017, 5:40:01 PM
 * Ver			: 1.0
 */
package com.harveynash.mvcmusicstorespringboot.api;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.util.UriComponentsBuilder;

import com.harveynash.mvcmusicstorespringboot.model.Album;
import com.harveynash.mvcmusicstorespringboot.service.AlbumService;

/**
 * @filename: AlbumApi.java
 * @author : Saitama Kenshin
 * @define : Api for all album request
 * 
 */
@RestController
@RequestMapping("/api")
public class AlbumApi {

	// Declare a field for dependency injection type AlbumService
	@Autowired
	private AlbumService albumService;

	// Get request mapping with url pattern '/album'
	@RequestMapping(value = { "/album/" }, method = RequestMethod.GET)
	public ResponseEntity<List<Album>> getAllAlbum() {

		// Get list of album from getAllAlbum method
		List<Album> album = albumService.getAllAlbum();

		// Check whether album is empty or not if yes then
		// return ResponseEntity with status NO_CONTENT
		if (album.isEmpty()) {
			return new ResponseEntity<List<Album>>(HttpStatus.NO_CONTENT);
		}

		// return ResponseEntity with list album and status OK
		return new ResponseEntity<List<Album>>(album, HttpStatus.OK);
	}

	// Get request mapping with url pattern '/album/{id}'
	@RequestMapping(value = { "/album/{id}" }, method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Album> findByAlbumId(@PathVariable("id") int id) {

		Album album = albumService.findByAlbumId(id);

		// Check whether album is null or not if yes then
		// return ResponseEntity with status NOT_FOUND
		if (album == null) {
			return new ResponseEntity<Album>(HttpStatus.NOT_FOUND);
		}

		// return ResponseEntity with album detail and status OK
		return new ResponseEntity<Album>(album, HttpStatus.OK);
	}

	// Get request mapping with url pattern '/admin/updateAlbum'
	@RequestMapping(value = { "/admin/album/" }, method = RequestMethod.POST)
	public ResponseEntity<Void> addAlbum(@RequestBody Album album, UriComponentsBuilder ucBuilder) {

		albumService.save(album);

		HttpHeaders header = new HttpHeaders();
		header.setLocation(ucBuilder.path("/admin/addAlbum/{id}").buildAndExpand(album.getAlbumId()).toUri());
		return new ResponseEntity<Void>(header, HttpStatus.CREATED);
	}

	// Get request mapping with url pattern '/admin/album/{id}'
	@RequestMapping(value = { "/admin/album/{id}" }, method = RequestMethod.PUT)
	public ResponseEntity<Album> updateAlbum(@PathVariable("id") int id, @RequestBody Album album) {

		Album currentAlbum = albumService.findByAlbumId(id);

		if (currentAlbum == null) {
			System.out.println("Album with id " + id + "not found");
			return new ResponseEntity<Album>(HttpStatus.NO_CONTENT);
		}

		currentAlbum.setAlbumArtUrl(album.getAlbumArtUrl());
		currentAlbum.setArtistId(album.getArtistId());
		currentAlbum.setGenreId(album.getGenreId());
		currentAlbum.setPrice(album.getPrice());
		currentAlbum.setTitle(album.getTitle());

		albumService.update(currentAlbum);

		return new ResponseEntity<Album>(currentAlbum, HttpStatus.OK);

	}

	// Get request mapping with url pattern '/admin/album/{id}'
	@RequestMapping(value = { "/admin/album/{id}" }, method = RequestMethod.DELETE)
	public ResponseEntity<Album> deleteAlbum(@PathVariable("id") int id, @RequestBody Album album) {
		// Delete album from delete method
		albumService.delete(id);

		return new ResponseEntity<Album>(HttpStatus.NO_CONTENT);
	}

}
