/**********************************************************************
 * Copyright (c) 2017, SaitamaSoft, All rights reserved. 
 ********************************************************************** 
 * 
 * Project Name : MvcMusicStore_SpringBoot
 * Date 		: Dec 14, 2017, 5:40:01 PM
 * Ver			: 1.0
 */
package com.harveynash.mvcmusicstorespringboot.api;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.harveynash.mvcmusicstorespringboot.service.UserService;

/**
 * @filename: UserApi.java
 * @author : Saitama Kenshin
 * @define : Api for all user request
 * 
 */
@RestController
@RequestMapping("/api")
public class UserApi {

	@Autowired
	UserService user;

}
