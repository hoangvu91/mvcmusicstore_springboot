/**********************************************************************
 * Copyright (c) 2017, SaitamaSoft, All rights reserved. 
 ********************************************************************** 
 * 
 * Project Name : MvcMusicStore_SpringBoot
 * Date 		: Nov 22, 2017, 10:44:11 AM
 * Ver			: 1.0
 */
package com.harveynash.mvcmusicstorespringboot.model;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnore;

/**
 * @filename: Artist.java
 * @author	: Saitama Kenshin
 * @define	: 
 * 
 */
@Entity
@Table(name = "artist")
public class Artist {
	
	@Id
	@GeneratedValue
	@Column(name = "ArtistId")
	private Integer artistId;
	
	@Column(name = "Name", length = 120, nullable = true)
	private String artistName;
	
	@JsonIgnore
	@OneToMany(mappedBy = "artist", cascade = CascadeType.ALL)
	private List<Album> album;

	/**
	 * @return the artistId
	 */
	public Integer getArtistId() {
		return artistId;
	}

	/**
	 * @param artistId the artistId to set
	 */
	public void setArtistId(Integer artistId) {
		this.artistId = artistId;
	}

	/**
	 * @return the artistName
	 */
	public String getArtistName() {
		return artistName;
	}

	/**
	 * @param artistName the artistName to set
	 */
	public void setArtistName(String artistName) {
		this.artistName = artistName;
	}

	/**
	 * @return the album
	 */
	public List<Album> getAlbum() {
		return album;
	}

	/**
	 * @param album the album to set
	 */
	public void setAlbum(List<Album> album) {
		this.album = album;
	}
}
