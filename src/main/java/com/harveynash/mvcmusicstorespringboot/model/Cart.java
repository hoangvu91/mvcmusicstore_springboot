/**********************************************************************
 * Copyright (c) 2017, SaitamaSoft, All rights reserved. 
 ********************************************************************** 
 * 
 * Project Name : MvcMusicStoreMaven
 * Date 		: Nov 17, 2017, 11:25:36 AM
 * Ver			: 1.0
 */
package com.harveynash.mvcmusicstorespringboot.model;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

/**
 * @filename: Cart.java
 * @author	: Saitama Kenshin
 * @define	: 
 * 
 */
public class Cart implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private Map<Album, Integer> items;
	private Integer albumId;
	private Integer quantity = 0;
	
	/**
	 * @return the items
	 */
	public Map<Album, Integer> getItems() {
		return items;
	}

	/**
	 * @param items the items to set
	 */
	public void setItems(Map<Album, Integer> items) {
		this.items = items;
	}

	/**
	 * @return the albumId
	 */
	public Integer getAlbumId() {
		return albumId;
	}

	/**
	 * @param albumId the albumId to set
	 */
	public void setAlbumId(Integer albumId) {
		this.albumId = albumId;
	}

	/**
	 * @return the quantity
	 */
	public Integer getQuantity() {
		return quantity;
	}

	/**
	 * @param quantity the quantity to set
	 */
	public void setQuantity(Integer quantity) {
		this.quantity = quantity;
	}

    public void addItem(Album album) {
        if (items == null) {
            items = new HashMap<Album, Integer>();
        }
        
        int storeQuantity = 1;
        
        if (items.containsKey(album)) {
        	storeQuantity = items.get(album) + 1;

        }
        items.put(album, storeQuantity);
    }
    
    public void editItem(Album album, int quantity) {
        if (items == null) {
            items = new HashMap<Album, Integer>();
        }
        
        items.put(album, quantity);
    }

    public void removeItem(Album album) {
        if (items == null) {
            return;
        }
        if (items.containsKey(album)) {
            items.remove(album);
        }
        if (items.isEmpty()) {
            items = null;
        }
    }
}
