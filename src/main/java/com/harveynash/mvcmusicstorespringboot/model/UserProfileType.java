/**********************************************************************
 * Copyright (c) 2017, SaitamaSoft, All rights reserved. 
 ********************************************************************** 
 * 
 * Project Name : MvcMusicStoreMaven
 * Date 		: Nov 13, 2017, 4:25:44 PM
 * Ver			: 1.0
 */
package com.harveynash.mvcmusicstorespringboot.model;

/**
 * @filename: UserProfileType.java
 * @author	: Saitama Kenshin
 * @define	: 
 * 
 */
public enum UserProfileType {
	USER("USER"),
    ADMIN("ADMIN");
     
    String userProfileType;
     
    private UserProfileType(String userProfileType){
        this.userProfileType = userProfileType;
    }
     
    public String getUserProfileType(){
        return userProfileType;
    }
}
