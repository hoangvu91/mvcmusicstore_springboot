/**********************************************************************
 * Copyright (c) 2017, SaitamaSoft, All rights reserved. 
 ********************************************************************** 
 * 
 * Project Name : MvcMusicStore_SpringBoot
 * Date 		: Nov 22, 2017, 10:42:31 AM
 * Ver			: 1.0
 */
package com.harveynash.mvcmusicstorespringboot.model;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnore;

/**
 * @filename: Genre.java
 * @author	: Saitama Kenshin
 * @define	: 
 * 
 */
@Entity
@Table(name = "genre")
public class Genre {

	@Id
	@GeneratedValue
	@Column(name = "GenreId")
	private Integer genreId;

	@Column(name = "Name", length = 120, nullable = true)
	private String genreName;

	@Column(name = "Description", length = 4000, nullable = true)
	private String description;

	@JsonIgnore
	@OneToMany(mappedBy = "genre", cascade = CascadeType.ALL)
	private List<Album> album;

	/**
	 * @return the genreId
	 */
	public Integer getGenreId() {
		return genreId;
	}

	/**
	 * @param genreId the genreId to set
	 */
	public void setGenreId(Integer genreId) {
		this.genreId = genreId;
	}

	/**
	 * @return the genreName
	 */
	public String getGenreName() {
		return genreName;
	}

	/**
	 * @param genreName the genreName to set
	 */
	public void setGenreName(String genreName) {
		this.genreName = genreName;
	}

	/**
	 * @return the description
	 */
	public String getDescription() {
		return description;
	}

	/**
	 * @param description the description to set
	 */
	public void setDescription(String description) {
		this.description = description;
	}

	/**
	 * @return the album
	 */
	public List<Album> getAlbum() {
		return album;
	}

	/**
	 * @param album the album to set
	 */
	public void setAlbum(List<Album> album) {
		this.album = album;
	}
}