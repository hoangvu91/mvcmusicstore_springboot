/**********************************************************************
 * Copyright (c) 2017, SaitamaSoft, All rights reserved. 
 ********************************************************************** 
 * 
 * Project Name : MvcMusicStoreMaven
 * Date 		: Nov 15, 2017, 4:49:03 PM
 * Ver			: 1.0
 */
package com.harveynash.mvcmusicstorespringboot.model;

import org.springframework.web.multipart.MultipartFile;

/**
 * @filename: FileBucket.java
 * @author	: Saitama Kenshin
 * @define	: 
 * 
 */
public class FileBucket {
	
	MultipartFile file;
    
    public MultipartFile getFile() {
        return file;
    }
 
    public void setFile(MultipartFile file) {
        this.file = file;
    }
}
