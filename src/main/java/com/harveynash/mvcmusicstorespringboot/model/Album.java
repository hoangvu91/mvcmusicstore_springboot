/**********************************************************************
 * Copyright (c) 2017, SaitamaSoft, All rights reserved. 
 ********************************************************************** 
 * 
 * Project Name : MvcMusicStore_SpringBoot
 * Date 		: Nov 22, 2017, 10:43:23 AM
 * Ver			: 1.0
 */
package com.harveynash.mvcmusicstorespringboot.model;

import java.io.Serializable;
import java.math.BigDecimal;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

/**
 * @filename: Album.java
 * @author : Saitama Kenshin
 * @define :
 * 
 */
@Entity
@Table(name = "album")
public class Album implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue
	@Column(name = "AlbumId")
	private Integer albumId;

	@Column(name = "GenreId")
	private Integer genreId;

	@Column(name = "ArtistId")
	private Integer artistId;

	
	@Column(name = "Title", length = 160, nullable = false)
	private String title;

	@Column(name = "Price")
	private BigDecimal price;

	@Column(name = "AlbumArtUrl", length = 1024, nullable = true)
	private String albumArtUrl;

	@ManyToOne()
	@JoinColumn(name = "GenreId", referencedColumnName = "GenreId", insertable = false, updatable = false)
	private Genre genre;

	@ManyToOne()
	@JoinColumn(name = "ArtistId", referencedColumnName = "ArtistId", insertable = false, updatable = false)
	private Artist artist;

	/**
	 * @return the albumId
	 */
	public Integer getAlbumId() {
		return albumId;
	}

	/**
	 * @param albumId
	 *            the albumId to set
	 */
	public void setAlbumId(Integer albumId) {
		this.albumId = albumId;
	}

	/**
	 * @return the genreId
	 */
	public Integer getGenreId() {
		return genreId;
	}

	/**
	 * @param genreId
	 *            the genreId to set
	 */
	public void setGenreId(Integer genreId) {
		this.genreId = genreId;
	}

	/**
	 * @return the artistId
	 */
	public Integer getArtistId() {
		return artistId;
	}

	/**
	 * @param artistId
	 *            the artistId to set
	 */
	public void setArtistId(Integer artistId) {
		this.artistId = artistId;
	}

	/**
	 * @return the title
	 */
	public String getTitle() {
		return title;
	}

	/**
	 * @param title
	 *            the title to set
	 */
	public void setTitle(String title) {
		this.title = title;
	}

	/**
	 * @return the price
	 */
	public BigDecimal getPrice() {
		return price;
	}

	/**
	 * @param price
	 *            the price to set
	 */
	public void setPrice(BigDecimal price) {
		this.price = price;
	}

	/**
	 * @return the albumArtUrl
	 */
	public String getAlbumArtUrl() {
		return albumArtUrl;
	}

	/**
	 * @param albumArtUrl
	 *            the albumArtUrl to set
	 */
	public void setAlbumArtUrl(String albumArtUrl) {
		this.albumArtUrl = albumArtUrl;
	}

	/**
	 * @return the genre
	 */
	public Genre getGenre() {
		return genre;
	}

	/**
	 * @param genre
	 *            the genre to set
	 */
	public void setGenre(Genre genre) {
		this.genre = genre;
	}

	/**
	 * @return the artist
	 */
	public Artist getArtist() {
		return artist;
	}

	/**
	 * @param artist
	 *            the artist to set
	 */
	public void setArtist(Artist artist) {
		this.artist = artist;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((albumId == null) ? 0 : albumId.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}

		if (obj == null) {
			return false;
		}

		if (getClass() != obj.getClass()) {
			return false;
		}

		Album other = (Album) obj;

		if (albumId == null) {

			if (other.albumId != null) {
				return false;
			}

		} else if (!albumId.equals(other.albumId)) {
			return false;
		}

		return true;
	}
}