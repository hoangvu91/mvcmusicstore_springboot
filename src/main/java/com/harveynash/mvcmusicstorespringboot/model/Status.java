/**********************************************************************
 * Copyright (c) 2017, SaitamaSoft, All rights reserved. 
 ********************************************************************** 
 * 
 * Project Name : MvcMusicStoreMaven
 * Date 		: Nov 13, 2017, 4:26:48 PM
 * Ver			: 1.0
 */
package com.harveynash.mvcmusicstorespringboot.model;

/**
 * @filename: Status.java
 * @author	: Saitama Kenshin
 * @define	: 
 * 
 */
public enum Status {
 
    ACTIVE("Active"),
    INACTIVE("Inactive"),
    DELETED("Deleted"),
    LOCKED("Locked");
     
    private String status;
     
    private Status(final String status){
        this.status = status;
    }
     
    public String getStatus(){
        return this.status;
    }
 
    @Override
    public String toString(){
        return this.status;
    }
 
    public String getName(){
        return this.name();
    }
}