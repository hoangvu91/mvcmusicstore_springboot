/**********************************************************************
 * Copyright (c) 2017, SaitamaSoft, All rights reserved. 
 ********************************************************************** 
 * 
 * Project Name : MvcMusicStore_SpringBoot
 * Date 		: Nov 29, 2017, 10:25:13 PM
 * Ver			: 1.0
 */
package com.harveynash.mvcmusicstorespringboot.model;

import java.io.Serializable;
import java.math.BigDecimal;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

/**
 * @filename: OrderDetail.java
 * @author	: Saitama Kenshin
 * @define	: 
 * 
 */
@Entity
@Table(name = "orderdetail")
public class OrderDetail implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue
	@Column(name = "OrderDetailId")
	private Integer orderDetailId;	
	
	@Column(name = "OrderId")
	private Integer OrderId;
	
	@ManyToOne()
	@JoinColumn(name = "OrderId", referencedColumnName = "OrderId", insertable = false, updatable = false)
	private Order order;
	
	@Column(name = "AlbumId")
	private Integer albumId;
	
	@Column(name = "Quantity")
	private Integer quantity;
	
	@Column(name = "UnitPrice")
	private BigDecimal unitPrice;

	/**
	 * @return the orderDetailId
	 */
	public Integer getOrderDetailId() {
		return orderDetailId;
	}

	/**
	 * @param orderDetailId the orderDetailId to set
	 */
	public void setOrderDetailId(Integer orderDetailId) {
		this.orderDetailId = orderDetailId;
	}

	/**
	 * @return the orderId
	 */
	public Integer getOrderId() {
		return OrderId;
	}

	/**
	 * @param orderId the orderId to set
	 */
	public void setOrderId(Integer orderId) {
		OrderId = orderId;
	}

	/**
	 * @return the order
	 */
	public Order getOrder() {
		return order;
	}

	/**
	 * @param order the order to set
	 */
	public void setOrder(Order order) {
		this.order = order;
	}

	/**
	 * @return the albumId
	 */
	public Integer getAlbumId() {
		return albumId;
	}

	/**
	 * @param albumId the albumId to set
	 */
	public void setAlbumId(Integer albumId) {
		this.albumId = albumId;
	}

	/**
	 * @return the quantity
	 */
	public Integer getQuantity() {
		return quantity;
	}

	/**
	 * @param quantity the quantity to set
	 */
	public void setQuantity(Integer quantity) {
		this.quantity = quantity;
	}

	/**
	 * @return the unitPrice
	 */
	public BigDecimal getUnitPrice() {
		return unitPrice;
	}

	/**
	 * @param unitPrice the unitPrice to set
	 */
	public void setUnitPrice(BigDecimal unitPrice) {
		this.unitPrice = unitPrice;
	}
}
