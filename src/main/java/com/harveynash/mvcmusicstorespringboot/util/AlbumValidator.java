/**********************************************************************
 * Copyright (c) 2017, SaitamaSoft, All rights reserved. 
 ********************************************************************** 
 * 
 * Project Name : MvcMusicStore_SpringBoot
 * Date 		: Nov 24, 2017, 12:31:50 AM
 * Ver			: 1.0
 */
package com.harveynash.mvcmusicstorespringboot.util;

import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;
import com.harveynash.mvcmusicstorespringboot.model.Album;

/**
 * @filename: AlbumValidator.java
 * @author	: Saitama Kenshin
 * @define	: 
 * 
 */
@Component
public class AlbumValidator implements Validator {

	@Override
	public boolean supports(Class<?> c) {
		return c.equals(Album.class);
	}

	@Override
	public void validate(Object target, Errors errors) {
		ValidationUtils.rejectIfEmpty(errors, "title", "NotEmpty.album.title");
		ValidationUtils.rejectIfEmpty(errors, "genreId", "NotEmpty.album.genreId");
		ValidationUtils.rejectIfEmpty(errors, "artistId", "NotEmpty.album.artistId");
		ValidationUtils.rejectIfEmpty(errors, "price", "NotEmpty.album.price");
		ValidationUtils.rejectIfEmpty(errors, "albumArtUrl", "NotEmpty.album.albumArtUrl");
	}
}
