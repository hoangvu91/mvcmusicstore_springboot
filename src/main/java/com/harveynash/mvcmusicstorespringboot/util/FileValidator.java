/**********************************************************************
 * Copyright (c) 2017, SaitamaSoft, All rights reserved. 
 ********************************************************************** 
 * 
 * Project Name : MvcMusicStoreMaven
 * Date 		: Nov 15, 2017, 4:57:54 PM
 * Ver			: 1.0
 */
package com.harveynash.mvcmusicstorespringboot.util;

import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;

import com.harveynash.mvcmusicstorespringboot.model.FileBucket;

/**
 * @filename: FileValidator.java
 * @author	: Saitama Kenshin
 * @define	: 
 *  
 */
@Component
public class FileValidator implements Validator {
         
    public boolean supports(Class<?> clazz) {
        return FileBucket.class.isAssignableFrom(clazz);
    }
 
    public void validate(Object obj, Errors errors) {
        FileBucket file = (FileBucket) obj;
             
        if(file.getFile()!=null){
            if (file.getFile().getSize() == 0) {
                errors.rejectValue("file", "missing.file");
            }
        }
    }
}
