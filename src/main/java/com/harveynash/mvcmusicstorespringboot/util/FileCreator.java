/**********************************************************************
 * Copyright (c) 2017, SaitamaSoft, All rights reserved. 
 ********************************************************************** 
 * 
 * Project Name : MvcMusicStoreMaven
 * Date 		: Nov 18, 2017, 8:52:49 PM
 * Ver			: 1.0
 */
package com.harveynash.mvcmusicstorespringboot.util;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.file.Files;

import org.springframework.stereotype.Component;
import org.springframework.web.multipart.MultipartFile;

/**
 * @filename: FileCreator.java
 * @author : Saitama Kenshin
 * @define : Factory for create file
 * 
 */
@Component
public class FileCreator {

	private static final String IMAGES = "images";

	private static final String IMAGES_PATH = IMAGES;

	private static final File IMAGES_DIR = new File(IMAGES_PATH);

	private static final String IMAGES_DIR_ABSOLUTE_PATH = IMAGES_DIR.getAbsolutePath() + File.separator;

	public byte[] getImages(String imageName, String ext) throws IOException {

		createDir();

		File serverFile = new File(IMAGES_DIR_ABSOLUTE_PATH + imageName + "." + ext);

		return Files.readAllBytes(serverFile.toPath());
	}

	public void createImage(String name, MultipartFile file) {

		try {
			System.out.println(IMAGES_DIR_ABSOLUTE_PATH);
			createDir();
			File image = new File(IMAGES_DIR_ABSOLUTE_PATH + name);
			BufferedOutputStream stream = new BufferedOutputStream(new FileOutputStream(image));
			stream.write(file.getBytes());
			stream.close();

		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private void createDir() {
		if (!IMAGES_DIR.exists()) {
			IMAGES_DIR.mkdirs();
		}
	}
}
