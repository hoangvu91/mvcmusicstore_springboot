/**********************************************************************
 * Copyright (c) 2017, SaitamaSoft, All rights reserved. 
 ********************************************************************** 
 * 
 * Project Name : MvcMusicStore_SpringBoot
 * Date 		: Dec 1, 2017, 1:19:35 AM
 * Ver			: 1.0
 */
package com.harveynash.mvcmusicstorespringboot.service;

import com.harveynash.mvcmusicstorespringboot.model.OrderDetail;

/**
 * @filename: OrderDetailService.java
 * @author	: Saitama Kenshin
 * @define	: 
 * 
 */
public interface OrderDetailService {

	//Method create new orderdetail
	void save(OrderDetail orderDetail);
}
