/**********************************************************************
 * Copyright (c) 2017, SaitamaSoft, All rights reserved. 
 ********************************************************************** 
 * 
 * Project Name : MvcMusicStoreMaven
 * Date 		: Nov 13, 2017, 1:41:22 PM
 * Ver			: 1.0
 */
package com.harveynash.mvcmusicstorespringboot.service;

import com.harveynash.mvcmusicstorespringboot.model.User;

/**
 * @filename: UserService.java
 * @author : Saitama Kenshin
 * @define : All methods declaration of UserService
 * 
 */
public interface UserService {

	// Method find user by id
	User findByUserId(int id);
	
    // Method get id of newly added user
    User getNewlyAddedUserId();
    
	// Method find user by username
    User findByUserName(String username);
    
    // Method create new user
    void save(User user);
}
