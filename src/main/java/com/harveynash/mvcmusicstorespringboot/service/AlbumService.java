/**********************************************************************
 * Copyright (c) 2017, SaitamaSoft, All rights reserved. 
 ********************************************************************** 
 * 
 * Project Name : MvcMusicStoreMaven
 * Date 		: Nov 12, 2017, 3:56:27 PM
 * Ver			: 1.0
 */
package com.harveynash.mvcmusicstorespringboot.service;

import java.util.List;

import com.harveynash.mvcmusicstorespringboot.model.Album;

/**
 * @filename: AlbumService.java
 * @author : Saitama Kenshin
 * @define : All method declaration of AlbumBo
 * 
 */
public interface AlbumService {

	// Method find album by id
	Album findByAlbumId(int id);
	
	// Method find album by title
	Album findByAlbumTitle(String title);

	// Method check album is unique or not
	boolean isAlbumUnique(Integer id, String title);
	
	// Method get newly added Album
	List<Album> getNewlyAddedAlbum();

	// Method get all album
	List<Album> getAllAlbum();
	
	// Method get album by genre id
	List<Album> getAlbumByGenreId(int id);

	// Method insert new album
	void save(Album album);

	// Method update an album
	void update(Album album);

	// Method delete album
	void delete(int id);
}
