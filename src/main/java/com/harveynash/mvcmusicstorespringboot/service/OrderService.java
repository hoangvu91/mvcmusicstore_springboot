/**********************************************************************
 * Copyright (c) 2017, SaitamaSoft, All rights reserved. 
 ********************************************************************** 
 * 
 * Project Name : MvcMusicStore_SpringBoot
 * Date 		: Nov 29, 2017, 10:56:05 PM
 * Ver			: 1.0
 */
package com.harveynash.mvcmusicstorespringboot.service;

import com.harveynash.mvcmusicstorespringboot.model.Order;

/**
 * @filename: OrderService.java
 * @author	: Saitama Kenshin
 * @define	: 
 * 
 */
public interface OrderService {

    // Method create new order
    void save(Order order);
}
