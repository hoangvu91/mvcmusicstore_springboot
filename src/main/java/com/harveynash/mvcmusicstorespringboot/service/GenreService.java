/**********************************************************************
 * Copyright (c) 2017, SaitamaSoft, All rights reserved. 
 ********************************************************************** 
 * 
 * Project Name : MvcMusicStoreMaven
 * Date 		: Nov 12, 2017, 12:41:48 AM
 * Ver			: 1.0
 */
package com.harveynash.mvcmusicstorespringboot.service;

import java.util.List;

import com.harveynash.mvcmusicstorespringboot.model.Genre;

/**
 * @filename: GenreService.java
 * @author : Saitama Kenshin
 * @define : All method declaration of GenreService
 * 
 */
public interface GenreService {

	// Method get album by album id
	Genre findByGenreId(int id);

	// Method get list genre
	List<Genre> findAllGenre();
}
