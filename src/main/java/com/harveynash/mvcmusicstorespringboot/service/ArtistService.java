/**********************************************************************
 * Copyright (c) 2017, SaitamaSoft, All rights reserved. 
 ********************************************************************** 
 * 
 * Project Name : MvcMusicStoreMaven
 * Date 		: Nov 15, 2017, 11:09:51 AM
 * Ver			: 1.0
 */
package com.harveynash.mvcmusicstorespringboot.service;

import java.util.List;

import com.harveynash.mvcmusicstorespringboot.model.Artist;


/**
 * @filename: ArtistService.java
 * @author : Saitama Kenshin
 * @define :
 * 
 */
public interface ArtistService {
	// Method get all artist
	List<Artist> getAllArtist();
}
