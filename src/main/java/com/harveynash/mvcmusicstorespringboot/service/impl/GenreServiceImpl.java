/**********************************************************************
 * Copyright (c) 2017, SaitamaSoft, All rights reserved. 
 ********************************************************************** 
 * 
 * Project Name : MvcMusicStoreMaven
 * Date 		: Nov 12, 2017, 12:42:56 AM
 * Ver			: 1.0
 */
package com.harveynash.mvcmusicstorespringboot.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.harveynash.mvcmusicstorespringboot.dao.GenreDao;
import com.harveynash.mvcmusicstorespringboot.model.Genre;
import com.harveynash.mvcmusicstorespringboot.service.GenreService;

/**
 * @filename: GenreServiceImpl.java
 * @author	: Saitama Kenshin
 * @define	: All methods implementation of GenreService
 * 
 */
@Service("GenreService")
@Transactional
public class GenreServiceImpl implements GenreService{

	// Declare a bean field for injection type GenreDao
	@Autowired
    private GenreDao genreDao;
	
	// Method return all genre
	public List<Genre> findAllGenre() {
		return genreDao.findAllGenre();
	}

	// Method find by genre id
	public Genre findByGenreId(int id) {
		return genreDao.findByGenreId(id);
	}
}
