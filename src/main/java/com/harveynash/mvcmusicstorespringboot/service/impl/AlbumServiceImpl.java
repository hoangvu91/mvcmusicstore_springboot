/**********************************************************************
 * Copyright (c) 2017, SaitamaSoft, All rights reserved. 
 ********************************************************************** 
 * 
 * Project Name : MvcMusicStoreMaven
 * Date 		: Nov 12, 2017, 3:57:33 PM
 * Ver			: 1.0
 */
package com.harveynash.mvcmusicstorespringboot.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.harveynash.mvcmusicstorespringboot.dao.AlbumDao;
import com.harveynash.mvcmusicstorespringboot.model.Album;
import com.harveynash.mvcmusicstorespringboot.service.AlbumService;

/**
 * @filename: AlbumServiceImpl.java
 * @author	: Saitama Kenshin
 * @define	: All methods implementation of AlbumBo
 * 
 */
@Service("AlbumService")
@Transactional
public class AlbumServiceImpl implements AlbumService{
	
	// Declare a bean field for injection type AlbumDao
	@Autowired
	private AlbumDao albumDao;

	// Method get album by key
	public Album findByAlbumId(int id) {
		return albumDao.findByAlbumId(id);
	}
	
	// Method get album by album title
	@Override
	public Album findByAlbumTitle(String title) {
		return albumDao.findByAlbumTitle(title);
	}
	
	// Method check album is unique or not
	@Override
	public boolean isAlbumUnique(Integer id, String title) {
		Album album = findByAlbumTitle(title);
		return (album == null || ((id != null) && (album.getAlbumId() == id)));
	}
	
	// Method get newly album
	public List<Album> getNewlyAddedAlbum() {
		return albumDao.getNewlyAddedAlbum();
	}

	// Method get all album
	public List<Album> getAllAlbum() {
		return albumDao.getAllAlbum();
	}
	
	
	// Method get album by genre id
	public List<Album> getAlbumByGenreId(int id) {
		return albumDao.getAlbumByGenreId(id);
	}

	// Method add new album
	public void save(Album album) {
		albumDao.save(album);
	}
	
	/*
     * Since the method is running with Transaction, No need to call hibernate update explicitly.
     * Just fetch the entity from db and update it with proper values within transaction.
     * It will be updated in db once transaction ends. 
     */
	public void update(Album album) {
		Album entity = albumDao.findByAlbumId(album.getAlbumId());
		
		if (entity != null) {
			entity.setGenreId(album.getGenreId());
			entity.setArtistId(album.getArtistId());
			entity.setTitle(album.getTitle());
			entity.setPrice(album.getPrice());
			entity.setAlbumArtUrl(album.getAlbumArtUrl());
		}
	}

	// Method delete an album by album id
	public void delete(int id) {
		albumDao.delete(id);
	}
}