/**********************************************************************
 * Copyright (c) 2017, SaitamaSoft, All rights reserved. 
 ********************************************************************** 
 * 
 * Project Name : MvcMusicStore_SpringBoot
 * Date 		: Nov 29, 2017, 10:57:19 PM
 * Ver			: 1.0
 */
package com.harveynash.mvcmusicstorespringboot.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.harveynash.mvcmusicstorespringboot.dao.OrderDao;
import com.harveynash.mvcmusicstorespringboot.model.Order;
import com.harveynash.mvcmusicstorespringboot.service.OrderService;

/**
 * @filename: OrderServiceImpl.java
 * @author	: Saitama Kenshin
 * @define	: 
 * 
 */
@Service("orderService")
@Transactional
public class OrderServiceImpl implements OrderService{

	@Autowired
	private OrderDao orderDao;
	
	// Method create new a order
	public void save(Order order) {
		orderDao.save(order);
	}
}
