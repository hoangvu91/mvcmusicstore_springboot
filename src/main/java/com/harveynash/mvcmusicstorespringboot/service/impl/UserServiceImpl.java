/**********************************************************************
 * Copyright (c) 2017, SaitamaSoft, All rights reserved. 
 ********************************************************************** 
 * 
 * Project Name : MvcMusicStoreMaven
 * Date 		: Nov 13, 2017, 4:49:06 PM
 * Ver			: 1.0
 */
package com.harveynash.mvcmusicstorespringboot.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.harveynash.mvcmusicstorespringboot.dao.UserDao;
import com.harveynash.mvcmusicstorespringboot.model.User;
import com.harveynash.mvcmusicstorespringboot.service.UserService;

/**
 * @filename: UserServiceImpl.java
 * @author : Saitama Kenshin
 * @define :
 * 
 */
@Service("userService")
@Transactional
public class UserServiceImpl implements UserService {
	
	@Autowired
	private UserDao userDao;

	// Method find user by id
	public User findByUserId(int id) {
		return userDao.findByUserId(id);
	}

	// Method find user by username
	public User findByUserName(String username) {
		return userDao.findByUserName(username);
	}

	// Method get id of newly added user
	public User getNewlyAddedUserId() {
		return userDao.getNewlyAddedUserId();
	}
	
	// Method create new user
	public void save(User user) {
		userDao.save(user);
	}
}
