/**********************************************************************
 * Copyright (c) 2017, SaitamaSoft, All rights reserved. 
 ********************************************************************** 
 * 
 * Project Name : MvcMusicStoreMaven
 * Date 		: Nov 15, 2017, 11:10:43 AM
 * Ver			: 1.0
 */
package com.harveynash.mvcmusicstorespringboot.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.harveynash.mvcmusicstorespringboot.dao.ArtistDao;
import com.harveynash.mvcmusicstorespringboot.model.Artist;
import com.harveynash.mvcmusicstorespringboot.service.ArtistService;

/**
 * @filename: ArtistServiceImpl.java
 * @author : Saitama Kenshin
 * @define :
 * 
 */
@Service("ArtistService")
@Transactional
public class ArtistServiceImpl implements ArtistService {

	// Declare a bean field for injection type AlbumDao
	@Autowired
	private ArtistDao artistDao;

	// Method get all artist
	public List<Artist> getAllArtist() {
		
		return artistDao.getAllArtist();
	}
}
