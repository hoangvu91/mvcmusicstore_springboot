/**********************************************************************
 * Copyright (c) 2017, SaitamaSoft, All rights reserved. 
 ********************************************************************** 
 * 
 * Project Name : MvcMusicStore_SpringBoot
 * Date 		: Dec 1, 2017, 1:21:10 AM
 * Ver			: 1.0
 */
package com.harveynash.mvcmusicstorespringboot.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.harveynash.mvcmusicstorespringboot.dao.OrderDetailDao;
import com.harveynash.mvcmusicstorespringboot.model.OrderDetail;
import com.harveynash.mvcmusicstorespringboot.service.OrderDetailService;

/**
 * @filename: OrderDetailServiceImpl.java
 * @author	: Saitama Kenshin
 * @define	: 
 * 
 */
@Service("orderDetailService")
@Transactional
public class OrderDetailServiceImpl implements OrderDetailService{

	@Autowired
	private OrderDetailDao orderDetailDao;
	
	public void save(OrderDetail orderDetail) {
		orderDetailDao.save(orderDetail);
	}

}
