jQuery(document).ready(function () {
	 // add the rule here
	$.validator.addMethod("valueNotEquals", function(value, element, arg){
		return arg != value;
	});
	
	$.validator.addMethod('positiveNumber', function (value) { 
		return Number(value) > 0;
    }),
    
    //Validate contact form on keyup and submit.
    $("#addNewAlbum").validate({
        rules: {
        	txtAlbumTitle: {
        		required: true,
                rangelength: [6, 160]
            },
            selectGenre: { 
            	valueNotEquals: "- Select Genre -" ,
    		},
    		selectArtist: { 
            	valueNotEquals: "- Select Artist -" ,
    		},
    		txtAlbumPrice: {
    			required: true,
    			number: true,
    			positiveNumber: true
            },
            file: {
            	required: true,
            	accept: "jpeg|jpg|png|gif|bmp|jfif",
            	
            }
        },
        messages: {
        	txtAlbumTitle: { required: "Please enter album title!" },
        	selectGenre: { valueNotEquals: "Please select a genre!" },
        	selectArtist: { valueNotEquals: "Please select an artist!" },
        	txtAlbumPrice: { required: "Please enter price!", positiveNumber: "Please enter positive number"},
        	file: { required: "Please select an image file!", accept: "Please enter file type with extension JPEG / JPG / PNG / GIF / BMP / JFIF!" }
        }
    });//End of validate
	
    //Validate contact form on keyup and submit.
    $("#editAlbum").validate({
        rules: {
        	title: {
        		required: true,
                rangelength: [6, 160]
            },
            selectGenre: { 
            	valueNotEquals: "- Select Genre -" ,
    		},
    		selectArtist: { 
            	valueNotEquals: "- Select Artist -" ,
    		},
    		price: {
    			required: true,
    			number: true,
    			positiveNumber: true
            },
            file: {
            	accept: "jpeg|jpg|png|gif|bmp|jfif",
            	
            }
        },
        messages: {
        	title: { required: "Please enter album title!" },
        	selectGenre: { valueNotEquals: "Please select a genre!" },
        	selectArtist: { valueNotEquals: "Please select an artist!" },
        	price: { required: "Please enter price!", positiveNumber: "Please enter positive number"},
        	file: { accept: "Please enter file type with extension JPEG / JPG / PNG / GIF / BMP / JFIF!" }
        }
    });//End of validate
    
    //Validate contact form on keyup and submit.
    $("#registerUser").validate({
        rules: {      	     	
        	txtUserName: {
                required: true,
                rangelength: [6, 50]
            },
            txtPassword: {
                required: true,
                rangelength: [6, 50]
            },
            txtRePassword: {
            	equalTo: "#txtPassword"
            },
            txtFirstName: {
                required: true,
                rangelength: [6, 50]
            },
            txtLastName: {
                required: true,
                rangelength: [6, 50]
            },
            txtAddress: {
                required: true,
                rangelength: [6, 70]
            },
            txtCity: {
                required: true,
                rangelength: [6, 40]
            },
            txtState: {
                required: true,
                rangelength: [6, 40]
            },
            txtPostalCode: {
            	required: true,
            	number: true
            },
            txtCountry: {
                required: true,
                rangelength: [6, 40]
            },
            txtPhone: {
            	required: true,
            	phone: true
            },
            txtEmail: {
            	required: true,
            	email: true
            }
        },
        messages: {
        	txtPhone: { phone: "Please enter phone with format (x)xxx-xxx-xxxx or (x)xxx xxx xxxx" },
        }
    });//End of validate
    
  //Validate contact form on keyup and submit.
    $("#checkout").validate({
        rules: {
        	txtAddress: {
                rangelength: [6, 160]
            },
            txtPhone: {
            	phone: true
            },
        },
        messages: {
        	txtPhone: { phone: "Please enter phone with format (x)xxx-xxx-xxxx or (x)xxx xxx xxxx" },
        }
    });//End of validate
});//End of function
