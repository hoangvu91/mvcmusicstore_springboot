<a class="btn btn-primary" href="${pageContext.request.contextPath}/admin/add" role="button">Add new</a>

<table class="table table-hover" style="width: 100%">
	<thead>
		<tr>
			<th>No.</th>
			<th>Title</th>
			<th>Genre</th>
			<th>Artist</th>			
			<th>Price</th>
			<th colspan="2"></th>
		</tr>
	</thead>
	<tbody>
		<c:set var="album" value="${requestScope.AlbumList}" />
		<c:if test="${not empty album}">
			<c:forEach var="row" items="${album}">
				<tr>
					<th scope="row">${row.albumId }</th>
					<td>${row.title }</td>
					<td>${row.getGenre().getGenreName() }</td>
					<td>${row.getArtist().getArtistName() }</td>
					<td>${row.price }</td>
					<td><a href="${pageContext.request.contextPath}/admin/edit?id=${row.albumId }">Edit</a></td>
					<td><a href="${pageContext.request.contextPath}/admin/delete?id=${row.albumId }">Delete</a></td>
				</tr>
			</c:forEach>
		</c:if>		
	</tbody>
</table>