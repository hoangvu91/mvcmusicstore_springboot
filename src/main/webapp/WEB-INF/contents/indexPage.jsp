<div id="promotion"></div>
<h3>
	<em>Fresh</em> off the grill
</h3>

<ul id="album-list">
	<c:set var="album" value="${requestScope.AlbumList}" />
	<c:if test="${not empty album}">
		<c:forEach var="row" items="${album}">
			<li>
			<a href="${pageContext.request.contextPath}/albumDetail?id=${row.albumId}">
				<c:if test="${not empty row.albumArtUrl }">
					<img src="${pageContext.request.contextPath}/images/${row.albumArtUrl}" height="100px" width="100px"/> 
					<span>${row.title }</span>
				</c:if>
				<c:if test="${empty row.albumArtUrl }">
					<img src="${pageContext.request.contextPath}/images/placeholder.gif" height="75px" width="100px"/> 
					<span>${row.title }</span>
				</c:if>
			</a>
			</li>
		</c:forEach>
	</c:if>
</ul>