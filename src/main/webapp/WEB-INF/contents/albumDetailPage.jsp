<form action="${pageContext.request.contextPath}/addToCart" method="post">
	
	<div id="album-cover">
		<c:if test="${not empty Album.albumArtUrl }">
			<img class="img-thumbnail" src="${pageContext.request.contextPath}/images/${Album.albumArtUrl}"/>
		</c:if>
		
		<c:if test="${empty Album.albumArtUrl }">
			<img class="img-thumbnail" src="${pageContext.request.contextPath}/images/placeholder.gif"/>
		</c:if>
	</div>
	
	<input type="hidden" name="txtAlbumId" value="${Album.albumId }">
	
	<div class="form-group">
		<label class="col-form-label" for="formGroupExampleInput">Album	Title</label> 
		<input type="text" readonly="readonly" class="form-control" name="txtTitle" value="${Album.title }">
	</div>

	<div class="form-group">
		<label class="col-form-label" for="formGroupExampleInput2">Album Genre</label> 
		<input type="text" disabled="disabled" class="form-control" value="${Album.genre.genreName }">
	</div>

	<div class="form-group">
		<label class="col-form-label" for="formGroupExampleInput2">Album Artist</label>
		<input type="text" disabled="disabled" class="form-control" value="${Album.artist.artistName }">	
	</div>

	<div class="form-group">
		<label class="col-form-label" for="formGroupExampleInput2">Price</label>
		<input type="text" disabled="disabled" class="form-control" value="${Album.price }">
	</div>
	
	<button type="submit" class="btn btn-primary">Add to cart</button>
</form>