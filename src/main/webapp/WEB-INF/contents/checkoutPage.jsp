<h2>Shipping to</h2>
<hr>
 <form id="checkout" action="${pageContext.request.contextPath}/checkout" method="POST">
 	<input type="hidden" name="txtTotal" value="${total}">
	<div class="form-group">
		<label class="col-form-label" for="formGroupExampleInput">Address</label> 
		<input type="text" class="form-control" name="txtAddress" placeholder="Address input">
	</div>
	
	<div class="form-group">
		<label class="col-form-label" for="formGroupExampleInput">Phone</label> 
		<input type="text" class="form-control" name="txtPhone" placeholder="Phone input">
	</div>	
	
	<button type="submit" class="btn btn-primary">Order now</button>
</form>