<table id="cart" class="table table-hover table-condensed">
	<thead>
		<tr>
			<th style="width:50%">Title</th>
			<th style="width:10%">Price</th>
			<th style="width:12%">Quantity</th>
			<th style="width:18%" class="text-center">Subtotal</th>
			<th style="width:10%"></th>
		</tr>
	</thead>			
	<tbody>
	<c:set var="total" value="${0}"/>
	<c:forEach var="item" items="${LISTCART}" varStatus="counter">
	<tr>
		<td data-th="Product">
			<div class="row">
				<div class="col-sm-2 hidden-xs">
					<a href="${pageContext.request.contextPath}/albumDetail?id=${item.key.albumId}">
						<c:if test="${not empty item.key.albumArtUrl}">
							<img src="${pageContext.request.contextPath}/images/${item.key.albumArtUrl}" class="img-responsive" 
									height="100px" width="100px"/> 
						</c:if>
						<c:if test="${empty item.key.albumArtUrl }">
							<img src="${pageContext.request.contextPath}/images/placeholder.gif" class="img-responsive" 
								height="75px" width="100px"/> 
						</c:if>
					</a>
				</div>							
				<div class="col-sm-10">
					<h4 class="nomargin">Album ${counter.count}</h4>
					<a href="${pageContext.request.contextPath}/albumDetail?id=${item.key.albumId}">
						<span>${item.key.title}</span>
					</a>
				</div>
			</div>
		</td>
		
		<td data-th="Price">${item.key.price}</td>
		<td>
			<form action="${pageContext.request.contextPath}/editCart" method="post">
			<div style="display: inline-flex;">
			<input type="hidden" name="txtAlbumId" value="${item.key.albumId }">
			
			<input type="number" min="1" class="form-control text-center quantity" id="txtQuantity"
				name="txtQuantity" value="${item.value}">
				
			<button name="btnEdit" class="btn btn-info btn-sm"><i class="fa fa-refresh"></i></button>
			</div>			
			
		</form>
		</td>
		<td data-th="Subtotal" class="text-center">
			<c:set var="subTotal" value="${item.key.price * item.value}"/>
			<c:out value = "${subTotal}" />
			<c:set var="total" value="${total + subTotal}"/>
		</td>
		<td class="actions" data-th="">			
			<a class="btn btn-danger btn-sm" 
				href="${pageContext.request.contextPath}/removeCart?id=${item.key.albumId}">
				<i class="fa fa-trash-o"></i>
			</a>								
		</td>
	</tr>
	
	</c:forEach>
	</tbody>
	<tfoot>
		<tr>
			<td>
				<a href="${pageContext.request.contextPath}" class="btn btn-warning">
					<i class="fa fa-angle-left"></i> Continue Shopping
				</a>
			</td>
			<td colspan="2" class="hidden-xs"></td>
			<td class="hidden-xs text-center">					
				<strong>Total $<c:out value = "${total}" /></strong>
			</td>
			<td>
				<a href="${pageContext.request.contextPath}/checkout?total=${total}" class="btn btn-success btn-block">
					Checkout <i class="fa fa-angle-right"></i></a>
			</td>
		</tr>
	</tfoot>
</table>
