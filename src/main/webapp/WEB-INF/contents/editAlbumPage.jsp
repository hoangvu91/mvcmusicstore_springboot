<form:form name="editAlbum" id="editAlbum" method="POST" action="${pageContext.request.contextPath}/admin/updateAlbum" 
	modelAttribute="album" enctype="multipart/form-data">
	
	<form:hidden path="albumId" />
	<form:hidden path="albumArtUrl" />
	
	<div id="album-cover">
		<c:if test="${not empty album.albumArtUrl }">
			<img class="img-thumbnail" src="${pageContext.request.contextPath}/images/${album.albumArtUrl}"/>
		</c:if>
	
		<c:if test="${empty album.albumArtUrl }">
			<img class="img-thumbnail" src="${pageContext.request.contextPath}/images/placeholder.gif"/>
		</c:if>
	</div>
	
	<div class="form-group">
		<label class="col-form-label" for="formGroupExampleInput">Album	Title</label> 
		<form:input path="title" id="title" type="text" name="txtAlbumTitle" class="form-control" 
			placeholder="Album title input" />
			
		<form:errors path="title" cssClass="text-danger"></form:errors>
	</div>
            
	<div class="form-group">
		<label class="col-form-label" for="formGroupExampleInput2">Album Genre</label> 
		<form:select path="genreId" name="selectGenre" id="genre" class="form-control" 
			style="width: 300px;">
			<form:options items="${genres_list}" itemValue="genreId" itemLabel="genreName"></form:options>
		</form:select>
	</div>
	
	<div class="form-group">
		<label class="col-form-label" for="formGroupExampleInput2">Album Artist</label> 
		<form:select path="artistId" name="selectArtist" id="artist" class="form-control" 
			style="width: 300px;">
			<form:options items="${artists_list}" itemValue="artistId" itemLabel="artistName"></form:options>
		</form:select>
	</div>
	
	<div class="form-group">
		<label class="col-form-label" for="formGroupExampleInput2">Price</label>
		<form:input path="price" type="number" name="txtAlbumPrice" class="form-control" id="price" 
			min="0.01" step="0.01" placeholder="Price input" style="width: 300px;"/>
		<form:errors path="price" cssClass="text-danger"></form:errors>
	</div>	
	
	<div class="form-group">
		<label for="lblAlbumCover">Album Cover</label>		
		<input type="file" id="fileupload" class="form-control-file" name="file">
		<div id="dvPreview"></div>
	</div>
	
	<button type="submit" class="btn btn-primary">Save</button>

</form:form>