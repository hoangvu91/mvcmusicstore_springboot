<form name="registerUser" id="registerUser" action="${pageContext.request.contextPath}/register" method="post">
		
	<div class="form-group">
		<label class="col-form-label" for="formGroupExampleInput">Username</label> 
		<input type="text" class="form-control" name="txtUserName" placeholder="Username input">
	</div>

	<div class="form-group">
		<label class="col-form-label" for="formGroupExampleInput2">Password</label> 
		<input type="password" class="form-control" name="txtPassword" placeholder="Password" id="txtPassword">

	</div>
	
	<div class="form-group">
		<label class="col-form-label" for="formGroupExampleInput2">Retype Password</label> 
		<input type="password" class="form-control" name="txtRePassword" placeholder="Password">

	</div>

	<div class="form-group">
		<label class="col-form-label" for="formGroupExampleInput">First Name</label> 
		<input type="text" class="form-control" name="txtFirstName" placeholder="First name input">
	</div>

	<div class="form-group">
		<label class="col-form-label" for="formGroupExampleInput">Last Name</label> 
		<input type="text" class="form-control" name="txtLastName" placeholder="Last name input">
	</div>

	<div class="form-group">
		<label class="col-form-label" for="formGroupExampleInput">Address</label> 
		<input type="text" class="form-control" name="txtAddress" placeholder="Last name input">
	</div>
	
	<div class="form-group">
		<label class="col-form-label" for="formGroupExampleInput">City</label> 
		<input type="text" class="form-control" name="txtCity" placeholder="City input">
	</div>
	
	<div class="form-group">
		<label class="col-form-label" for="formGroupExampleInput">State</label> 
		<input type="text" class="form-control" name="txtState" placeholder="State input">
	</div>
	
	<div class="form-group">
		<label class="col-form-label" for="formGroupExampleInput">Postal Code</label> 
		<input type="text" class="form-control" name="txtPostalCode" placeholder="Postal code input">
	</div>

	<div class="form-group">
		<label class="col-form-label" for="formGroupExampleInput">Country</label> 
		<input type="text" class="form-control" name="txtCountry" placeholder="Country input">
	</div>	

	<div class="form-group">
		<label class="col-form-label" for="formGroupExampleInput">Phone</label> 
		<input type="text" class="form-control" name="txtPhone" placeholder="Phone input">
	</div>	

	<div class="form-group">
		<label class="col-form-label" for="formGroupExampleInput">Email</label> 
		<input type="text" class="form-control" name="txtEmail" placeholder="Email input">
	</div>
	
	<button type="submit" class="btn btn-primary">Save</button>
</form>