<form name="addNewAlbum" id="addNewAlbum" action="${pageContext.request.contextPath}/admin/saveAlbum" 
		enctype="multipart/form-data" method="post">
		
	<div class="form-group">
		<label class="col-form-label" for="formGroupExampleInput">Album	Title</label> 
		<input type="text" class="form-control" name="txtAlbumTitle" placeholder="Album title input">
	</div>

	<div class="form-group">
		<label class="col-form-label" for="formGroupExampleInput2">Album Genre</label> 

		<select name="selectGenre" class="form-control" style="width: 300px;">
			<option selected>- Select Genre -</option>
			<c:set var="genre" value="${requestScope.GenreList}" />
			<c:if test="${not empty genre}">
				<c:forEach var="row" items="${genre}">
					<option value="${row.genreId}">${row.genreName}</option>
				</c:forEach>
			</c:if>
		</select>

	</div>

	<div class="form-group">
		<label class="col-form-label" for="formGroupExampleInput2">Album Artist</label>

		<select name="selectArtist" class="form-control" style="width: 300px;">
			<option selected>- Select Artist -</option>
			<c:set var="artist" value="${requestScope.ArtistList}" />
			<c:if test="${not empty artist}">
				<c:forEach var="row" items="${artist}">
					<option value="${row.artistId}">${row.artistName}</option>
				</c:forEach>
			</c:if>
		</select>		
	</div>

	<div class="form-group">
		<label class="col-form-label" for="formGroupExampleInput2">Price</label>
		<input type="text" class="form-control" name="txtAlbumPrice" placeholder="Price input">
	</div>

	<div class="form-group">
		<label for="lblAlbumCover">Album Cover</label> 
		<input type="file" id="fileupload" class="form-control-file" name="file">
		<div id="dvPreview"></div>
	</div>

	<button type="submit" class="btn btn-primary">Save</button>

</form>