<ul id="categories">
	<c:set var="genre" value="${requestScope.GenreList}" />
	<c:if test="${not empty genre}">
		<c:forEach var="row" items="${genre}">
			<li><a href="${pageContext.request.contextPath}/Genre?id=${row.genreId}">${row.genreName}</a>
			</li>
		</c:forEach>
	</c:if>
</ul>