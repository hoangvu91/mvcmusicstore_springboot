<div id="header">
	<h1>
		<a href="${pageContext.request.contextPath}">JAVA SPRING MVC MUSIC STORE</a>
	</h1>
	<ul id="navlist">
		<li class="first"><a href="${pageContext.request.contextPath}"
			id="current">Home</a></li>
		<li><a href="${pageContext.request.contextPath}/shoppingCart">ShoppingCart</a></li>

		<c:if test="${empty pageContext.request.userPrincipal}">
			<li><a href="${pageContext.request.contextPath}/login">Login</a></li>
			<li><a href="${pageContext.request.contextPath}/register">Register</a></li>
		</c:if>

		<c:if test="${not empty pageContext.request.userPrincipal}">
			<sec:authorize access="hasRole('ADMIN')">
				<li><a href="${pageContext.request.contextPath}/admin/manageAlbum">Manage User</a></li>
       	 		<li><a href="${pageContext.request.contextPath}/admin/manageAlbum">Manage Album</a></li>
			</sec:authorize>
			
			<div id="welcome">
				Welcome	<c:out value="${pageContext.request.userPrincipal.name}" />, 
			<a href="<c:url value="/logout" />">Logout</a>
		</div>
		</c:if>
		
	</ul>
</div>