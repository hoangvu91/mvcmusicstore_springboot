<%@include file="../includes/_pageDeclare.jsp" %>

<html>
<head>
	<%@include file="../includes/_resources.jsp" %>
	<title>MVC Music Store - Access Denied Page</title>
</head>
<body>
	<%@include file="../includes/_header.jsp" %>

	<div id="main">
		<%@include file="../includes/_category.jsp" %>
		<%@include file="../contents/accessDeniedPage.jsp" %>
	</div>

	<%@include file="../includes/_footer.jsp" %>
</body>
</html>