<%@include file="../includes/_pageDeclare.jsp" %>

<html>
<head>
	<%@include file="../includes/_resources.jsp" %>
	<title>MVC Music Store - Manage Album Page</title>
</head>
<body>
	<%@include file="../includes/_header.jsp" %>

	<div id="main">
		<%@include file="../contents/manageAlbumPage.jsp" %>
	</div>

	<%@include file="../includes/_footer.jsp" %>
</body>
</html>