<%@include file="../includes/_pageDeclare.jsp"%>

<html>
<head>
	<%@include file="../includes/_resources.jsp"%>
	<title>MVC Music Store - Login Page</title>
</head>
<body>
	<%@include file="../includes/_header.jsp"%>

	<div id="main">
		<%@include file="../contents/loginPage.jsp"%>
	</div>

	<%@include file="../includes/_footer.jsp"%>
</body>
</html>