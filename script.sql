-- MySQL Workbench Forward Engineering

SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='TRADITIONAL,ALLOW_INVALID_DATES';

-- -----------------------------------------------------
-- Schema mydb
-- -----------------------------------------------------
-- -----------------------------------------------------
-- Schema mvcmusicstore
-- -----------------------------------------------------

-- -----------------------------------------------------
-- Schema mvcmusicstore
-- -----------------------------------------------------
CREATE SCHEMA IF NOT EXISTS `mvcmusicstore` DEFAULT CHARACTER SET utf8mb4 ;
USE `mvcmusicstore` ;

-- -----------------------------------------------------
-- Table `mvcmusicstore`.`genre`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `mvcmusicstore`.`genre` ;

CREATE TABLE IF NOT EXISTS `mvcmusicstore`.`genre` (
  `GenreId` INT(11) NOT NULL AUTO_INCREMENT,
  `Name` VARCHAR(120) CHARACTER SET 'utf8' NULL DEFAULT NULL,
  `Description` VARCHAR(4000) CHARACTER SET 'utf8' NULL DEFAULT NULL,
  PRIMARY KEY (`GenreId`))
ENGINE = InnoDB
AUTO_INCREMENT = 11
DEFAULT CHARACTER SET = utf8mb4
COLLATE = utf8mb4_unicode_ci;


-- -----------------------------------------------------
-- Table `mvcmusicstore`.`artist`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `mvcmusicstore`.`artist` ;

CREATE TABLE IF NOT EXISTS `mvcmusicstore`.`artist` (
  `ArtistId` INT(11) NOT NULL AUTO_INCREMENT,
  `Name` VARCHAR(120) CHARACTER SET 'utf8' NULL DEFAULT NULL,
  PRIMARY KEY (`ArtistId`))
ENGINE = InnoDB
AUTO_INCREMENT = 279
DEFAULT CHARACTER SET = utf8mb4
COLLATE = utf8mb4_unicode_ci;


-- -----------------------------------------------------
-- Table `mvcmusicstore`.`album`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `mvcmusicstore`.`album` ;

CREATE TABLE IF NOT EXISTS `mvcmusicstore`.`album` (
  `AlbumId` INT(11) NOT NULL AUTO_INCREMENT,
  `GenreId` INT(11) NOT NULL,
  `ArtistId` INT(11) NOT NULL,
  `Title` VARCHAR(160) CHARACTER SET 'utf8' NOT NULL,
  `Price` DECIMAL(10,2) NOT NULL,
  `AlbumArtUrl` VARCHAR(1024) CHARACTER SET 'utf8mb4' COLLATE 'utf8mb4_unicode_ci' NULL DEFAULT NULL,
  PRIMARY KEY (`AlbumId`),
  INDEX `FK_Album_Genre_idx` (`GenreId` ASC),
  INDEX `FK__Album__ArtistId__276EDEB3_idx` (`ArtistId` ASC),
  CONSTRAINT `FK_Album_Genre`
    FOREIGN KEY (`GenreId`)
    REFERENCES `mvcmusicstore`.`genre` (`GenreId`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `FK__Album__ArtistId__276EDEB3`
    FOREIGN KEY (`ArtistId`)
    REFERENCES `mvcmusicstore`.`artist` (`ArtistId`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
AUTO_INCREMENT = 680
DEFAULT CHARACTER SET = utf8mb4
COLLATE = utf8mb4_unicode_ci;


-- -----------------------------------------------------
-- Table `mvcmusicstore`.`cart`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `mvcmusicstore`.`cart` ;

CREATE TABLE IF NOT EXISTS `mvcmusicstore`.`cart` (
  `RecordId` INT(11) NOT NULL AUTO_INCREMENT,
  `CartId` VARCHAR(50) CHARACTER SET 'utf8mb4' COLLATE 'utf8mb4_unicode_ci' NOT NULL,
  `AlbumId` INT(11) NOT NULL,
  `Count` INT(11) NOT NULL,
  `DateCreated` DATETIME NOT NULL,
  PRIMARY KEY (`RecordId`),
  INDEX `FK_Cart_Album_idx` (`AlbumId` ASC),
  CONSTRAINT `FK_Cart_Album`
    FOREIGN KEY (`AlbumId`)
    REFERENCES `mvcmusicstore`.`album` (`AlbumId`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8mb4
COLLATE = utf8mb4_unicode_ci;


-- -----------------------------------------------------
-- Table `mvcmusicstore`.`order`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `mvcmusicstore`.`order` ;

CREATE TABLE IF NOT EXISTS `mvcmusicstore`.`order` (
  `OrderId` INT(11) NOT NULL AUTO_INCREMENT,
  `OrderDate` DATETIME NOT NULL,
  `Total` DECIMAL(10,2) NOT NULL,
  `UserId` VARCHAR(45) CHARACTER SET 'utf8mb4' COLLATE 'utf8mb4_unicode_ci' NOT NULL,
  PRIMARY KEY (`OrderId`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8mb4
COLLATE = utf8mb4_unicode_ci;


-- -----------------------------------------------------
-- Table `mvcmusicstore`.`orderdetail`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `mvcmusicstore`.`orderdetail` ;

CREATE TABLE IF NOT EXISTS `mvcmusicstore`.`orderdetail` (
  `OrderDetailId` INT(11) NOT NULL AUTO_INCREMENT,
  `OrderId` INT(11) NOT NULL,
  `AlbumId` INT(11) NOT NULL,
  `Quantity` INT(11) NOT NULL,
  `UnitPrice` DECIMAL(10,2) NOT NULL,
  PRIMARY KEY (`OrderDetailId`),
  INDEX `FK__InvoiceLi__Invoi__2F10007B_idx` (`OrderId` ASC),
  INDEX `FK_InvoiceLine_Album_idx` (`AlbumId` ASC),
  CONSTRAINT `FK_InvoiceLine_Album`
    FOREIGN KEY (`AlbumId`)
    REFERENCES `mvcmusicstore`.`album` (`AlbumId`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `FK__InvoiceLi__Invoi__2F10007B`
    FOREIGN KEY (`OrderId`)
    REFERENCES `mvcmusicstore`.`order` (`OrderId`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8mb4
COLLATE = utf8mb4_unicode_ci;


-- -----------------------------------------------------
-- Table `mvcmusicstore`.`user`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `mvcmusicstore`.`user` ;

CREATE TABLE IF NOT EXISTS `mvcmusicstore`.`user` (
  `UserId` BIGINT(20) NOT NULL AUTO_INCREMENT,
  `Username` VARCHAR(50) CHARACTER SET 'utf8mb4' COLLATE 'utf8mb4_unicode_ci' NOT NULL,
  `Password` VARCHAR(255) CHARACTER SET 'utf8mb4' COLLATE 'utf8mb4_unicode_ci' NOT NULL,
  `FirstName` VARCHAR(50) CHARACTER SET 'utf8mb4' COLLATE 'utf8mb4_unicode_ci' NULL DEFAULT NULL,
  `LastName` VARCHAR(50) CHARACTER SET 'utf8mb4' COLLATE 'utf8mb4_unicode_ci' NULL DEFAULT NULL,
  `Address` VARCHAR(70) CHARACTER SET 'utf8mb4' COLLATE 'utf8mb4_unicode_ci' NULL DEFAULT NULL,
  `City` VARCHAR(40) CHARACTER SET 'utf8mb4' COLLATE 'utf8mb4_unicode_ci' NULL DEFAULT NULL,
  `State` VARCHAR(40) CHARACTER SET 'utf8mb4' COLLATE 'utf8mb4_unicode_ci' NULL DEFAULT NULL,
  `PostalCode` VARCHAR(10) CHARACTER SET 'utf8mb4' COLLATE 'utf8mb4_unicode_ci' NULL DEFAULT NULL,
  `Country` VARCHAR(40) CHARACTER SET 'utf8mb4' COLLATE 'utf8mb4_unicode_ci' NULL DEFAULT NULL,
  `Phone` VARCHAR(24) CHARACTER SET 'utf8mb4' COLLATE 'utf8mb4_unicode_ci' NULL DEFAULT NULL,
  `Email` VARCHAR(50) CHARACTER SET 'utf8mb4' COLLATE 'utf8mb4_unicode_ci' NULL DEFAULT NULL,
  `Status` VARCHAR(45) CHARACTER SET 'utf8mb4' COLLATE 'utf8mb4_unicode_ci' NULL DEFAULT 'Active',
  `Type` VARCHAR(45) CHARACTER SET 'utf8mb4' COLLATE 'utf8mb4_unicode_ci' NOT NULL DEFAULT 'USER',
  PRIMARY KEY (`UserId`),
  UNIQUE INDEX `username_UNIQUE` (`Username` ASC))
ENGINE = InnoDB
AUTO_INCREMENT = 11
DEFAULT CHARACTER SET = utf8mb4
COLLATE = utf8mb4_unicode_ci;


SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;

-- -----------------------------------------------------
-- Data for table `mvcmusicstore`.`genre`
-- -----------------------------------------------------
START TRANSACTION;
USE `mvcmusicstore`;
INSERT INTO `mvcmusicstore`.`genre` (`GenreId`, `Name`, `Description`) VALUES (GenreId, 'Name', 'Description');
INSERT INTO `mvcmusicstore`.`genre` (`GenreId`, `Name`, `Description`) VALUES (1, 'Rock', 'Rock and Roll is a form of rock music developed in the 1950s and 1960s. Rock music combines many kinds of music from the United States, such as country music, folk music, church music, work songs, blues and jazz.');
INSERT INTO `mvcmusicstore`.`genre` (`GenreId`, `Name`, `Description`) VALUES (2, 'Jazz', 'Jazz is a type of music which was invented in the United States. Jazz music combines African-American music with European music. Some common jazz instruments include the saxophone, trumpet, piano, double bass, and drums.');
INSERT INTO `mvcmusicstore`.`genre` (`GenreId`, `Name`, `Description`) VALUES (3, 'Heavy Metal', 'Heavy Metal is a loud, aggressive style of Rock music. The bands who play heavy-metal music usually have one or two guitars, a bass guitar and drums. In some bands, electronic keyboards, organs, or other instruments are used. Heavy metal songs are loud and powerful-sounding, and have strong rhythms that are repeated. There are many different types of Heavy Metal, some of which are described below. Heavy metal bands sometimes dress in jeans, leather jackets, and leather boots, and have long hair. Heavy metal bands sometimes behave in a dramatic way when they play their instruments or sing. However, many heavy metal bands do not like to do this.');
INSERT INTO `mvcmusicstore`.`genre` (`GenreId`, `Name`, `Description`) VALUES (4, 'Alternative', 'Alternative rock is a type of rock music that became popular in the 1980s and became widely popular in the 1990s. Alternative rock is made up of various subgenres that have come out of the indie music scene since the 1980s, such as grunge, indie rock, Britpop, gothic rock, and indie pop. These genres are sorted by their collective types of punk, which laid the groundwork for alternative music in the 1970s.');
INSERT INTO `mvcmusicstore`.`genre` (`GenreId`, `Name`, `Description`) VALUES (5, 'Disco', 'Disco is a style of pop music that was popular in the mid-1970s. Disco music has a strong beat that people can dance to. People usually dance to disco music at bars called disco clubs. The word disco\" is also used to refer to the style of dancing that people do to disco music');
INSERT INTO `mvcmusicstore`.`genre` (`GenreId`, `Name`, `Description`) VALUES (6, 'The Blues', 'The blues is a form of music that started in the United States during the start of the 20th century. It was started by former African slaves from spirituals, praise songs, and chants. The first blues songs were called Delta blues. These songs came from the area near the mouth of the Mississippi River.');
INSERT INTO `mvcmusicstore`.`genre` (`GenreId`, `Name`, `Description`) VALUES (7, 'Latin American', 'Latin American music is the music of all countries in Latin America (and the Caribbean) and comes in many varieties. Latin America is home to musical styles such as the simple, rural conjunto music of northern Mexico, the sophisticated habanera of Cuba, the rhythmic sounds of the Puerto Rican plena, the symphonies of Heitor Villa-Lobos, and the simple and moving Andean flute. Music has played an important part recently in Latin America\'s politics, the nueva canción movement being a prime example. Latin music is very diverse, with the only truly unifying thread being the use of Latin-derived languages, predominantly the Spanish language, the Portuguese language in Brazil, and to a lesser extent, Latin-derived creole languages, such as those found in Haiti.');
INSERT INTO `mvcmusicstore`.`genre` (`GenreId`, `Name`, `Description`) VALUES (8, 'Reggae', 'Reggae is a music genre first developed in Jamaica in the late 1960s. While sometimes used in a broader sense to refer to most types of Jamaican music, the term reggae more properly denotes a particular music style that originated following on the development of ska and rocksteady.');
INSERT INTO `mvcmusicstore`.`genre` (`GenreId`, `Name`, `Description`) VALUES (9, 'Pop', 'Pop music is a music genre that developed from the mid-1950s as a softer alternative to rock \'\' roll and later to rock music. It has a focus on commercial recording, often oriented towards a youth market, usually through the medium of relatively short and simple love songs. While these basic elements of the genre have remained fairly constant, pop music has absorbed influences from most other forms of popular music, particularly borrowing from the development of rock music, and utilizing key technological innovations to produce new variations on existing themes.');
INSERT INTO `mvcmusicstore`.`genre` (`GenreId`, `Name`, `Description`) VALUES (10, 'Classical', 'Classical music is a very general term which normally refers to the standard music of countries in the Western world. It is music that has been composed by musicians who are trained in the art of writing music (composing) and written down in music notation so that other musicians can play it. Classical music can also be described as art music\" because great art (skill) is needed to compose it and to perform it well. Classical music differs from pop music because it is not made just in order to be popular for a short time or just to be a commercial success.\"');

COMMIT;


-- -----------------------------------------------------
-- Data for table `mvcmusicstore`.`artist`
-- -----------------------------------------------------
START TRANSACTION;
USE `mvcmusicstore`;
INSERT INTO `mvcmusicstore`.`artist` (`ArtistId`, `Name`) VALUES (ArtistId, 'Name');
INSERT INTO `mvcmusicstore`.`artist` (`ArtistId`, `Name`) VALUES (1, 'AC/DC');
INSERT INTO `mvcmusicstore`.`artist` (`ArtistId`, `Name`) VALUES (2, 'Accept');
INSERT INTO `mvcmusicstore`.`artist` (`ArtistId`, `Name`) VALUES (3, 'Aerosmith');
INSERT INTO `mvcmusicstore`.`artist` (`ArtistId`, `Name`) VALUES (4, 'Alanis Morissette');
INSERT INTO `mvcmusicstore`.`artist` (`ArtistId`, `Name`) VALUES (5, 'Alice In Chains');
INSERT INTO `mvcmusicstore`.`artist` (`ArtistId`, `Name`) VALUES (6, 'Antônio Carlos Jobim');
INSERT INTO `mvcmusicstore`.`artist` (`ArtistId`, `Name`) VALUES (7, 'Apocalyptica');
INSERT INTO `mvcmusicstore`.`artist` (`ArtistId`, `Name`) VALUES (8, 'Audioslave');
INSERT INTO `mvcmusicstore`.`artist` (`ArtistId`, `Name`) VALUES (10, 'Billy Cobham');
INSERT INTO `mvcmusicstore`.`artist` (`ArtistId`, `Name`) VALUES (11, 'Black Label Society');
INSERT INTO `mvcmusicstore`.`artist` (`ArtistId`, `Name`) VALUES (12, 'Black Sabbath');
INSERT INTO `mvcmusicstore`.`artist` (`ArtistId`, `Name`) VALUES (14, 'Bruce Dickinso');
INSERT INTO `mvcmusicstore`.`artist` (`ArtistId`, `Name`) VALUES (15, 'Buddy Guy');
INSERT INTO `mvcmusicstore`.`artist` (`ArtistId`, `Name`) VALUES (16, 'Caetano Veloso');
INSERT INTO `mvcmusicstore`.`artist` (`ArtistId`, `Name`) VALUES (17, 'Chico Buarque');
INSERT INTO `mvcmusicstore`.`artist` (`ArtistId`, `Name`) VALUES (18, 'Chico Science & Nação Zumbi');
INSERT INTO `mvcmusicstore`.`artist` (`ArtistId`, `Name`) VALUES (19, 'Cidade Negra');
INSERT INTO `mvcmusicstore`.`artist` (`ArtistId`, `Name`) VALUES (20, 'Cláudio Zoli');
INSERT INTO `mvcmusicstore`.`artist` (`ArtistId`, `Name`) VALUES (21, 'Various Artists');
INSERT INTO `mvcmusicstore`.`artist` (`ArtistId`, `Name`) VALUES (22, 'Led Zeppeli');
INSERT INTO `mvcmusicstore`.`artist` (`ArtistId`, `Name`) VALUES (23, 'Frank Zappa & Captain Beefheart');
INSERT INTO `mvcmusicstore`.`artist` (`ArtistId`, `Name`) VALUES (24, 'Marcos Valle');
INSERT INTO `mvcmusicstore`.`artist` (`ArtistId`, `Name`) VALUES (27, 'Gilberto Gil');
INSERT INTO `mvcmusicstore`.`artist` (`ArtistId`, `Name`) VALUES (37, 'Ed Motta');
INSERT INTO `mvcmusicstore`.`artist` (`ArtistId`, `Name`) VALUES (41, 'Elis Regina');
INSERT INTO `mvcmusicstore`.`artist` (`ArtistId`, `Name`) VALUES (42, 'Milton Nascimento');
INSERT INTO `mvcmusicstore`.`artist` (`ArtistId`, `Name`) VALUES (46, 'Jorge Be');
INSERT INTO `mvcmusicstore`.`artist` (`ArtistId`, `Name`) VALUES (50, 'Metallica');
INSERT INTO `mvcmusicstore`.`artist` (`ArtistId`, `Name`) VALUES (51, 'Quee');
INSERT INTO `mvcmusicstore`.`artist` (`ArtistId`, `Name`) VALUES (52, 'Kiss');
INSERT INTO `mvcmusicstore`.`artist` (`ArtistId`, `Name`) VALUES (53, 'Spyro Gyra');
INSERT INTO `mvcmusicstore`.`artist` (`ArtistId`, `Name`) VALUES (55, 'David Coverdale');
INSERT INTO `mvcmusicstore`.`artist` (`ArtistId`, `Name`) VALUES (56, 'Gonzaguinha');
INSERT INTO `mvcmusicstore`.`artist` (`ArtistId`, `Name`) VALUES (58, 'Deep Purple');
INSERT INTO `mvcmusicstore`.`artist` (`ArtistId`, `Name`) VALUES (59, 'Santana');
INSERT INTO `mvcmusicstore`.`artist` (`ArtistId`, `Name`) VALUES (68, 'Miles Davis');
INSERT INTO `mvcmusicstore`.`artist` (`ArtistId`, `Name`) VALUES (72, 'Vinícius De Moraes');
INSERT INTO `mvcmusicstore`.`artist` (`ArtistId`, `Name`) VALUES (76, 'Creedence Clearwater Revival');
INSERT INTO `mvcmusicstore`.`artist` (`ArtistId`, `Name`) VALUES (77, 'Cássia Eller');
INSERT INTO `mvcmusicstore`.`artist` (`ArtistId`, `Name`) VALUES (79, 'Dennis Chambers');
INSERT INTO `mvcmusicstore`.`artist` (`ArtistId`, `Name`) VALUES (80, 'Djava');
INSERT INTO `mvcmusicstore`.`artist` (`ArtistId`, `Name`) VALUES (81, 'Eric Clapto');
INSERT INTO `mvcmusicstore`.`artist` (`ArtistId`, `Name`) VALUES (82, 'Faith No More');
INSERT INTO `mvcmusicstore`.`artist` (`ArtistId`, `Name`) VALUES (83, 'Falamansa');
INSERT INTO `mvcmusicstore`.`artist` (`ArtistId`, `Name`) VALUES (84, 'Foo Fighters');
INSERT INTO `mvcmusicstore`.`artist` (`ArtistId`, `Name`) VALUES (86, 'Funk Como Le Gusta');
INSERT INTO `mvcmusicstore`.`artist` (`ArtistId`, `Name`) VALUES (87, 'Godsmack');
INSERT INTO `mvcmusicstore`.`artist` (`ArtistId`, `Name`) VALUES (88, 'Guns \' Roses');
INSERT INTO `mvcmusicstore`.`artist` (`ArtistId`, `Name`) VALUES (89, 'Incognito');
INSERT INTO `mvcmusicstore`.`artist` (`ArtistId`, `Name`) VALUES (90, 'Iron Maide');
INSERT INTO `mvcmusicstore`.`artist` (`ArtistId`, `Name`) VALUES (92, 'Jamiroquai');
INSERT INTO `mvcmusicstore`.`artist` (`ArtistId`, `Name`) VALUES (94, 'Jimi Hendrix');
INSERT INTO `mvcmusicstore`.`artist` (`ArtistId`, `Name`) VALUES (95, 'Joe Satriani');
INSERT INTO `mvcmusicstore`.`artist` (`ArtistId`, `Name`) VALUES (96, 'Jota Quest');
INSERT INTO `mvcmusicstore`.`artist` (`ArtistId`, `Name`) VALUES (98, 'Judas Priest');
INSERT INTO `mvcmusicstore`.`artist` (`ArtistId`, `Name`) VALUES (99, 'Legião Urbana');
INSERT INTO `mvcmusicstore`.`artist` (`ArtistId`, `Name`) VALUES (100, 'Lenny Kravitz');
INSERT INTO `mvcmusicstore`.`artist` (`ArtistId`, `Name`) VALUES (101, 'Lulu Santos');
INSERT INTO `mvcmusicstore`.`artist` (`ArtistId`, `Name`) VALUES (102, 'Marillio');
INSERT INTO `mvcmusicstore`.`artist` (`ArtistId`, `Name`) VALUES (103, 'Marisa Monte');
INSERT INTO `mvcmusicstore`.`artist` (`ArtistId`, `Name`) VALUES (105, 'Men At Work');
INSERT INTO `mvcmusicstore`.`artist` (`ArtistId`, `Name`) VALUES (106, 'Motörhead');
INSERT INTO `mvcmusicstore`.`artist` (`ArtistId`, `Name`) VALUES (109, 'Mötley Crüe');
INSERT INTO `mvcmusicstore`.`artist` (`ArtistId`, `Name`) VALUES (110, 'Nirvana');
INSERT INTO `mvcmusicstore`.`artist` (`ArtistId`, `Name`) VALUES (111, 'O Terço');
INSERT INTO `mvcmusicstore`.`artist` (`ArtistId`, `Name`) VALUES (112, 'Olodum');
INSERT INTO `mvcmusicstore`.`artist` (`ArtistId`, `Name`) VALUES (113, 'Os Paralamas Do Sucesso');
INSERT INTO `mvcmusicstore`.`artist` (`ArtistId`, `Name`) VALUES (114, 'Ozzy Osbourne');
INSERT INTO `mvcmusicstore`.`artist` (`ArtistId`, `Name`) VALUES (115, 'Page & Plant');
INSERT INTO `mvcmusicstore`.`artist` (`ArtistId`, `Name`) VALUES (117, 'Paul D\'Ianno');
INSERT INTO `mvcmusicstore`.`artist` (`ArtistId`, `Name`) VALUES (118, 'Pearl Jam');
INSERT INTO `mvcmusicstore`.`artist` (`ArtistId`, `Name`) VALUES (120, 'Pink Floyd');
INSERT INTO `mvcmusicstore`.`artist` (`ArtistId`, `Name`) VALUES (124, 'R.E.M.');
INSERT INTO `mvcmusicstore`.`artist` (`ArtistId`, `Name`) VALUES (126, 'Raul Seixas');
INSERT INTO `mvcmusicstore`.`artist` (`ArtistId`, `Name`) VALUES (127, 'Red Hot Chili Peppers');
INSERT INTO `mvcmusicstore`.`artist` (`ArtistId`, `Name`) VALUES (128, 'Rush');
INSERT INTO `mvcmusicstore`.`artist` (`ArtistId`, `Name`) VALUES (130, 'Skank');
INSERT INTO `mvcmusicstore`.`artist` (`ArtistId`, `Name`) VALUES (132, 'Soundgarde');
INSERT INTO `mvcmusicstore`.`artist` (`ArtistId`, `Name`) VALUES (133, 'Stevie Ray Vaughan & Double Trouble');
INSERT INTO `mvcmusicstore`.`artist` (`ArtistId`, `Name`) VALUES (134, 'Stone Temple Pilots');
INSERT INTO `mvcmusicstore`.`artist` (`ArtistId`, `Name`) VALUES (135, 'System Of A Dow');
INSERT INTO `mvcmusicstore`.`artist` (`ArtistId`, `Name`) VALUES (136, 'Terry Bozzio, Tony Levin & Steve Stevens');
INSERT INTO `mvcmusicstore`.`artist` (`ArtistId`, `Name`) VALUES (137, 'The Black Crowes');
INSERT INTO `mvcmusicstore`.`artist` (`ArtistId`, `Name`) VALUES (139, 'The Cult');
INSERT INTO `mvcmusicstore`.`artist` (`ArtistId`, `Name`) VALUES (140, 'The Doors');
INSERT INTO `mvcmusicstore`.`artist` (`ArtistId`, `Name`) VALUES (141, 'The Police');
INSERT INTO `mvcmusicstore`.`artist` (`ArtistId`, `Name`) VALUES (142, 'The Rolling Stones');
INSERT INTO `mvcmusicstore`.`artist` (`ArtistId`, `Name`) VALUES (144, 'The Who');
INSERT INTO `mvcmusicstore`.`artist` (`ArtistId`, `Name`) VALUES (145, 'Tim Maia');
INSERT INTO `mvcmusicstore`.`artist` (`ArtistId`, `Name`) VALUES (150, 'U2');
INSERT INTO `mvcmusicstore`.`artist` (`ArtistId`, `Name`) VALUES (151, 'UB40');
INSERT INTO `mvcmusicstore`.`artist` (`ArtistId`, `Name`) VALUES (152, 'Van Hale');
INSERT INTO `mvcmusicstore`.`artist` (`ArtistId`, `Name`) VALUES (153, 'Velvet Revolver');
INSERT INTO `mvcmusicstore`.`artist` (`ArtistId`, `Name`) VALUES (155, 'Zeca Pagodinho');
INSERT INTO `mvcmusicstore`.`artist` (`ArtistId`, `Name`) VALUES (157, 'Dread Zeppeli');
INSERT INTO `mvcmusicstore`.`artist` (`ArtistId`, `Name`) VALUES (179, 'Scorpions');
INSERT INTO `mvcmusicstore`.`artist` (`ArtistId`, `Name`) VALUES (196, 'Cake');
INSERT INTO `mvcmusicstore`.`artist` (`ArtistId`, `Name`) VALUES (197, 'Aisha Duo');
INSERT INTO `mvcmusicstore`.`artist` (`ArtistId`, `Name`) VALUES (200, 'The Posies');
INSERT INTO `mvcmusicstore`.`artist` (`ArtistId`, `Name`) VALUES (201, 'Luciana Souza/Romero Lubambo');
INSERT INTO `mvcmusicstore`.`artist` (`ArtistId`, `Name`) VALUES (202, 'Aaron Goldberg');
INSERT INTO `mvcmusicstore`.`artist` (`ArtistId`, `Name`) VALUES (203, 'Nicolaus Esterhazy Sinfonia');
INSERT INTO `mvcmusicstore`.`artist` (`ArtistId`, `Name`) VALUES (204, 'Temple of the Dog');
INSERT INTO `mvcmusicstore`.`artist` (`ArtistId`, `Name`) VALUES (205, 'Chris Cornell');
INSERT INTO `mvcmusicstore`.`artist` (`ArtistId`, `Name`) VALUES (206, 'Alberto Turco & Nova Schola Gregoriana');
INSERT INTO `mvcmusicstore`.`artist` (`ArtistId`, `Name`) VALUES (208, 'English Concert & Trevor Pinnock');
INSERT INTO `mvcmusicstore`.`artist` (`ArtistId`, `Name`) VALUES (211, 'Wilhelm Kempff');
INSERT INTO `mvcmusicstore`.`artist` (`ArtistId`, `Name`) VALUES (212, 'Yo-Yo Ma');
INSERT INTO `mvcmusicstore`.`artist` (`ArtistId`, `Name`) VALUES (213, 'Scholars Baroque Ensemble');
INSERT INTO `mvcmusicstore`.`artist` (`ArtistId`, `Name`) VALUES (217, 'Royal Philharmonic Orchestra & Sir Thomas Beecham');
INSERT INTO `mvcmusicstore`.`artist` (`ArtistId`, `Name`) VALUES (219, 'Britten Sinfonia, Ivor Bolton & Lesley Garrett');
INSERT INTO `mvcmusicstore`.`artist` (`ArtistId`, `Name`) VALUES (221, 'Sir Georg Solti & Wiener Philharmoniker');
INSERT INTO `mvcmusicstore`.`artist` (`ArtistId`, `Name`) VALUES (223, 'London Symphony Orchestra & Sir Charles Mackerras');
INSERT INTO `mvcmusicstore`.`artist` (`ArtistId`, `Name`) VALUES (224, 'Barry Wordsworth & BBC Concert Orchestra');
INSERT INTO `mvcmusicstore`.`artist` (`ArtistId`, `Name`) VALUES (226, 'Eugene Ormandy');
INSERT INTO `mvcmusicstore`.`artist` (`ArtistId`, `Name`) VALUES (229, 'Boston Symphony Orchestra & Seiji Ozawa');
INSERT INTO `mvcmusicstore`.`artist` (`ArtistId`, `Name`) VALUES (230, 'Aaron Copland & London Symphony Orchestra');
INSERT INTO `mvcmusicstore`.`artist` (`ArtistId`, `Name`) VALUES (231, 'Ton Koopma');
INSERT INTO `mvcmusicstore`.`artist` (`ArtistId`, `Name`) VALUES (232, 'Sergei Prokofiev & Yuri Temirkanov');
INSERT INTO `mvcmusicstore`.`artist` (`ArtistId`, `Name`) VALUES (233, 'Chicago Symphony Orchestra & Fritz Reiner');
INSERT INTO `mvcmusicstore`.`artist` (`ArtistId`, `Name`) VALUES (234, 'Orchestra of The Age of Enlightenment');
INSERT INTO `mvcmusicstore`.`artist` (`ArtistId`, `Name`) VALUES (236, 'James Levine');
INSERT INTO `mvcmusicstore`.`artist` (`ArtistId`, `Name`) VALUES (237, 'Berliner Philharmoniker & Hans Rosbaud');
INSERT INTO `mvcmusicstore`.`artist` (`ArtistId`, `Name`) VALUES (238, 'Maurizio Pollini');
INSERT INTO `mvcmusicstore`.`artist` (`ArtistId`, `Name`) VALUES (240, 'Gustav Mahler');
INSERT INTO `mvcmusicstore`.`artist` (`ArtistId`, `Name`) VALUES (242, 'Edo de Waart & San Francisco Symphony');
INSERT INTO `mvcmusicstore`.`artist` (`ArtistId`, `Name`) VALUES (244, 'Choir Of Westminster Abbey & Simon Presto');
INSERT INTO `mvcmusicstore`.`artist` (`ArtistId`, `Name`) VALUES (245, 'Michael Tilson Thomas & San Francisco Symphony');
INSERT INTO `mvcmusicstore`.`artist` (`ArtistId`, `Name`) VALUES (247, 'The King\'s Singers');
INSERT INTO `mvcmusicstore`.`artist` (`ArtistId`, `Name`) VALUES (248, 'Berliner Philharmoniker & Herbert Von Karaja');
INSERT INTO `mvcmusicstore`.`artist` (`ArtistId`, `Name`) VALUES (250, 'Christopher O\'Riley');
INSERT INTO `mvcmusicstore`.`artist` (`ArtistId`, `Name`) VALUES (251, 'Fretwork');
INSERT INTO `mvcmusicstore`.`artist` (`ArtistId`, `Name`) VALUES (252, 'Amy Winehouse');
INSERT INTO `mvcmusicstore`.`artist` (`ArtistId`, `Name`) VALUES (253, 'Calexico');
INSERT INTO `mvcmusicstore`.`artist` (`ArtistId`, `Name`) VALUES (255, 'Yehudi Menuhi');
INSERT INTO `mvcmusicstore`.`artist` (`ArtistId`, `Name`) VALUES (258, 'Les Arts Florissants & William Christie');
INSERT INTO `mvcmusicstore`.`artist` (`ArtistId`, `Name`) VALUES (259, 'The 12 Cellists of The Berlin Philharmonic');
INSERT INTO `mvcmusicstore`.`artist` (`ArtistId`, `Name`) VALUES (260, 'Adrian Leaper & Doreen de Feis');
INSERT INTO `mvcmusicstore`.`artist` (`ArtistId`, `Name`) VALUES (261, 'Roger Norrington, London Classical Players');
INSERT INTO `mvcmusicstore`.`artist` (`ArtistId`, `Name`) VALUES (264, 'Kent Nagano and Orchestre de l\'Opéra de Lyo');
INSERT INTO `mvcmusicstore`.`artist` (`ArtistId`, `Name`) VALUES (265, 'Julian Bream');
INSERT INTO `mvcmusicstore`.`artist` (`ArtistId`, `Name`) VALUES (266, 'Martin Roscoe');
INSERT INTO `mvcmusicstore`.`artist` (`ArtistId`, `Name`) VALUES (267, 'Göteborgs Symfoniker & Neeme Järvi');
INSERT INTO `mvcmusicstore`.`artist` (`ArtistId`, `Name`) VALUES (270, 'Gerald Moore');
INSERT INTO `mvcmusicstore`.`artist` (`ArtistId`, `Name`) VALUES (271, 'Mela Tenenbaum, Pro Musica Prague & Richard Kapp');
INSERT INTO `mvcmusicstore`.`artist` (`ArtistId`, `Name`) VALUES (274, 'Nash Ensemble');
INSERT INTO `mvcmusicstore`.`artist` (`ArtistId`, `Name`) VALUES (276, 'Chic');
INSERT INTO `mvcmusicstore`.`artist` (`ArtistId`, `Name`) VALUES (277, 'Anita Ward');
INSERT INTO `mvcmusicstore`.`artist` (`ArtistId`, `Name`) VALUES (278, 'Donna Summer');

COMMIT;


-- -----------------------------------------------------
-- Data for table `mvcmusicstore`.`album`
-- -----------------------------------------------------
START TRANSACTION;
USE `mvcmusicstore`;
INSERT INTO `mvcmusicstore`.`album` (`AlbumId`, `GenreId`, `ArtistId`, `Title`, `Price`, `AlbumArtUrl`) VALUES (AlbumId, GenreId, ArtistId, 'Title', Price, 'AlbumArtUrl');
INSERT INTO `mvcmusicstore`.`album` (`AlbumId`, `GenreId`, `ArtistId`, `Title`, `Price`, `AlbumArtUrl`) VALUES (386, 1, 1, 'For Those About To Rock We Salute You', 8.99, null);
INSERT INTO `mvcmusicstore`.`album` (`AlbumId`, `GenreId`, `ArtistId`, `Title`, `Price`, `AlbumArtUrl`) VALUES (387, 1, 1, 'Let There Be Rock', 8.99, null);
INSERT INTO `mvcmusicstore`.`album` (`AlbumId`, `GenreId`, `ArtistId`, `Title`, `Price`, `AlbumArtUrl`) VALUES (388, 1, 100, 'Greatest Hits', 8.99, null);
INSERT INTO `mvcmusicstore`.`album` (`AlbumId`, `GenreId`, `ArtistId`, `Title`, `Price`, `AlbumArtUrl`) VALUES (389, 1, 102, 'Misplaced Childhood', 8.99, null);
INSERT INTO `mvcmusicstore`.`album` (`AlbumId`, `GenreId`, `ArtistId`, `Title`, `Price`, `AlbumArtUrl`) VALUES (390, 1, 105, 'The Best Of Men At Work', 8.99, null);
INSERT INTO `mvcmusicstore`.`album` (`AlbumId`, `GenreId`, `ArtistId`, `Title`, `Price`, `AlbumArtUrl`) VALUES (392, 1, 110, 'Nevermind', 8.99, null);
INSERT INTO `mvcmusicstore`.`album` (`AlbumId`, `GenreId`, `ArtistId`, `Title`, `Price`, `AlbumArtUrl`) VALUES (393, 1, 111, 'Compositores', 8.99, null);
INSERT INTO `mvcmusicstore`.`album` (`AlbumId`, `GenreId`, `ArtistId`, `Title`, `Price`, `AlbumArtUrl`) VALUES (394, 1, 114, 'Bark at the Moon (Remastered)', 8.99, null);
INSERT INTO `mvcmusicstore`.`album` (`AlbumId`, `GenreId`, `ArtistId`, `Title`, `Price`, `AlbumArtUrl`) VALUES (395, 1, 114, 'Blizzard of Ozz', 8.99, null);
INSERT INTO `mvcmusicstore`.`album` (`AlbumId`, `GenreId`, `ArtistId`, `Title`, `Price`, `AlbumArtUrl`) VALUES (396, 1, 114, 'Diary of a Madman (Remastered)', 8.99, null);
INSERT INTO `mvcmusicstore`.`album` (`AlbumId`, `GenreId`, `ArtistId`, `Title`, `Price`, `AlbumArtUrl`) VALUES (397, 1, 114, 'No More Tears (Remastered)', 8.99, null);
INSERT INTO `mvcmusicstore`.`album` (`AlbumId`, `GenreId`, `ArtistId`, `Title`, `Price`, `AlbumArtUrl`) VALUES (398, 1, 114, 'Speak of the Devil', 8.99, null);
INSERT INTO `mvcmusicstore`.`album` (`AlbumId`, `GenreId`, `ArtistId`, `Title`, `Price`, `AlbumArtUrl`) VALUES (399, 1, 115, 'Walking Into Clarksdale', 8.99, null);
INSERT INTO `mvcmusicstore`.`album` (`AlbumId`, `GenreId`, `ArtistId`, `Title`, `Price`, `AlbumArtUrl`) VALUES (400, 1, 117, 'The Beast Live', 8.99, null);
INSERT INTO `mvcmusicstore`.`album` (`AlbumId`, `GenreId`, `ArtistId`, `Title`, `Price`, `AlbumArtUrl`) VALUES (401, 1, 118, 'Live On Two Legs [Live', 8.99, null);
INSERT INTO `mvcmusicstore`.`album` (`AlbumId`, `GenreId`, `ArtistId`, `Title`, `Price`, `AlbumArtUrl`) VALUES (402, 1, 118, 'Riot Act', 8.99, null);
INSERT INTO `mvcmusicstore`.`album` (`AlbumId`, `GenreId`, `ArtistId`, `Title`, `Price`, `AlbumArtUrl`) VALUES (403, 1, 118, 'Te', 8.99, null);
INSERT INTO `mvcmusicstore`.`album` (`AlbumId`, `GenreId`, `ArtistId`, `Title`, `Price`, `AlbumArtUrl`) VALUES (404, 1, 118, 'Vs.', 8.99, null);
INSERT INTO `mvcmusicstore`.`album` (`AlbumId`, `GenreId`, `ArtistId`, `Title`, `Price`, `AlbumArtUrl`) VALUES (405, 1, 120, 'Dark Side Of The Moo', 8.99, null);
INSERT INTO `mvcmusicstore`.`album` (`AlbumId`, `GenreId`, `ArtistId`, `Title`, `Price`, `AlbumArtUrl`) VALUES (406, 1, 124, 'New Adventures In Hi-Fi', 8.99, null);
INSERT INTO `mvcmusicstore`.`album` (`AlbumId`, `GenreId`, `ArtistId`, `Title`, `Price`, `AlbumArtUrl`) VALUES (407, 1, 126, 'Raul Seixas', 8.99, null);
INSERT INTO `mvcmusicstore`.`album` (`AlbumId`, `GenreId`, `ArtistId`, `Title`, `Price`, `AlbumArtUrl`) VALUES (408, 1, 127, 'By The Way', 8.99, null);
INSERT INTO `mvcmusicstore`.`album` (`AlbumId`, `GenreId`, `ArtistId`, `Title`, `Price`, `AlbumArtUrl`) VALUES (409, 1, 127, 'Californicatio', 8.99, null);
INSERT INTO `mvcmusicstore`.`album` (`AlbumId`, `GenreId`, `ArtistId`, `Title`, `Price`, `AlbumArtUrl`) VALUES (410, 1, 128, 'Retrospective I (1974-1980)', 8.99, null);
INSERT INTO `mvcmusicstore`.`album` (`AlbumId`, `GenreId`, `ArtistId`, `Title`, `Price`, `AlbumArtUrl`) VALUES (411, 1, 130, 'Maquinarama', 8.99, null);
INSERT INTO `mvcmusicstore`.`album` (`AlbumId`, `GenreId`, `ArtistId`, `Title`, `Price`, `AlbumArtUrl`) VALUES (412, 1, 130, 'O Samba Poconé', 8.99, null);
INSERT INTO `mvcmusicstore`.`album` (`AlbumId`, `GenreId`, `ArtistId`, `Title`, `Price`, `AlbumArtUrl`) VALUES (413, 1, 132, 'A-Sides', 8.99, null);
INSERT INTO `mvcmusicstore`.`album` (`AlbumId`, `GenreId`, `ArtistId`, `Title`, `Price`, `AlbumArtUrl`) VALUES (414, 1, 134, 'Core', 8.99, null);
INSERT INTO `mvcmusicstore`.`album` (`AlbumId`, `GenreId`, `ArtistId`, `Title`, `Price`, `AlbumArtUrl`) VALUES (415, 1, 136, '[1997 Black Light Syndrome', 8.99, null);
INSERT INTO `mvcmusicstore`.`album` (`AlbumId`, `GenreId`, `ArtistId`, `Title`, `Price`, `AlbumArtUrl`) VALUES (416, 1, 139, 'Beyond Good And Evil', 8.99, null);
INSERT INTO `mvcmusicstore`.`album` (`AlbumId`, `GenreId`, `ArtistId`, `Title`, `Price`, `AlbumArtUrl`) VALUES (418, 1, 140, 'The Doors', 8.99, null);
INSERT INTO `mvcmusicstore`.`album` (`AlbumId`, `GenreId`, `ArtistId`, `Title`, `Price`, `AlbumArtUrl`) VALUES (419, 1, 141, 'The Police Greatest Hits', 8.99, null);
INSERT INTO `mvcmusicstore`.`album` (`AlbumId`, `GenreId`, `ArtistId`, `Title`, `Price`, `AlbumArtUrl`) VALUES (420, 1, 142, 'Hot Rocks, 1964-1971 (Disc 1)', 8.99, null);
INSERT INTO `mvcmusicstore`.`album` (`AlbumId`, `GenreId`, `ArtistId`, `Title`, `Price`, `AlbumArtUrl`) VALUES (421, 1, 142, 'No Security', 8.99, null);
INSERT INTO `mvcmusicstore`.`album` (`AlbumId`, `GenreId`, `ArtistId`, `Title`, `Price`, `AlbumArtUrl`) VALUES (422, 1, 142, 'Voodoo Lounge', 8.99, null);
INSERT INTO `mvcmusicstore`.`album` (`AlbumId`, `GenreId`, `ArtistId`, `Title`, `Price`, `AlbumArtUrl`) VALUES (423, 1, 144, 'My Generation - The Very Best Of The Who', 8.99, null);
INSERT INTO `mvcmusicstore`.`album` (`AlbumId`, `GenreId`, `ArtistId`, `Title`, `Price`, `AlbumArtUrl`) VALUES (424, 1, 150, 'Achtung Baby', 8.99, null);
INSERT INTO `mvcmusicstore`.`album` (`AlbumId`, `GenreId`, `ArtistId`, `Title`, `Price`, `AlbumArtUrl`) VALUES (425, 1, 150, 'B-Sides 1980-1990', 8.99, null);
INSERT INTO `mvcmusicstore`.`album` (`AlbumId`, `GenreId`, `ArtistId`, `Title`, `Price`, `AlbumArtUrl`) VALUES (426, 1, 150, 'How To Dismantle An Atomic Bomb', 8.99, null);
INSERT INTO `mvcmusicstore`.`album` (`AlbumId`, `GenreId`, `ArtistId`, `Title`, `Price`, `AlbumArtUrl`) VALUES (427, 1, 150, 'Pop', 8.99, null);
INSERT INTO `mvcmusicstore`.`album` (`AlbumId`, `GenreId`, `ArtistId`, `Title`, `Price`, `AlbumArtUrl`) VALUES (428, 1, 150, 'Rattle And Hum', 8.99, null);
INSERT INTO `mvcmusicstore`.`album` (`AlbumId`, `GenreId`, `ArtistId`, `Title`, `Price`, `AlbumArtUrl`) VALUES (429, 1, 150, 'The Best Of 1980-1990', 8.99, null);
INSERT INTO `mvcmusicstore`.`album` (`AlbumId`, `GenreId`, `ArtistId`, `Title`, `Price`, `AlbumArtUrl`) VALUES (430, 1, 150, 'War', 8.99, null);
INSERT INTO `mvcmusicstore`.`album` (`AlbumId`, `GenreId`, `ArtistId`, `Title`, `Price`, `AlbumArtUrl`) VALUES (431, 1, 150, 'Zooropa', 8.99, null);
INSERT INTO `mvcmusicstore`.`album` (`AlbumId`, `GenreId`, `ArtistId`, `Title`, `Price`, `AlbumArtUrl`) VALUES (432, 1, 152, 'Diver Dow', 8.99, null);
INSERT INTO `mvcmusicstore`.`album` (`AlbumId`, `GenreId`, `ArtistId`, `Title`, `Price`, `AlbumArtUrl`) VALUES (433, 1, 152, 'The Best Of Van Halen, Vol. I', 8.99, null);
INSERT INTO `mvcmusicstore`.`album` (`AlbumId`, `GenreId`, `ArtistId`, `Title`, `Price`, `AlbumArtUrl`) VALUES (434, 1, 152, 'Van Halen III', 8.99, null);
INSERT INTO `mvcmusicstore`.`album` (`AlbumId`, `GenreId`, `ArtistId`, `Title`, `Price`, `AlbumArtUrl`) VALUES (435, 1, 152, 'Van Hale', 8.99, null);
INSERT INTO `mvcmusicstore`.`album` (`AlbumId`, `GenreId`, `ArtistId`, `Title`, `Price`, `AlbumArtUrl`) VALUES (436, 1, 153, 'Contraband', 8.99, null);
INSERT INTO `mvcmusicstore`.`album` (`AlbumId`, `GenreId`, `ArtistId`, `Title`, `Price`, `AlbumArtUrl`) VALUES (437, 1, 157, 'Un-Led-Ed', 8.99, null);
INSERT INTO `mvcmusicstore`.`album` (`AlbumId`, `GenreId`, `ArtistId`, `Title`, `Price`, `AlbumArtUrl`) VALUES (439, 1, 2, 'Balls to the Wall', 8.99, null);
INSERT INTO `mvcmusicstore`.`album` (`AlbumId`, `GenreId`, `ArtistId`, `Title`, `Price`, `AlbumArtUrl`) VALUES (440, 1, 2, 'Restless and Wild', 8.99, null);
INSERT INTO `mvcmusicstore`.`album` (`AlbumId`, `GenreId`, `ArtistId`, `Title`, `Price`, `AlbumArtUrl`) VALUES (441, 1, 200, 'Every Kind of Light', 8.99, null);
INSERT INTO `mvcmusicstore`.`album` (`AlbumId`, `GenreId`, `ArtistId`, `Title`, `Price`, `AlbumArtUrl`) VALUES (442, 1, 22, 'BBC Sessions [Disc 1 [Live', 8.99, null);
INSERT INTO `mvcmusicstore`.`album` (`AlbumId`, `GenreId`, `ArtistId`, `Title`, `Price`, `AlbumArtUrl`) VALUES (443, 1, 22, 'BBC Sessions [Disc 2 [Live', 8.99, null);
INSERT INTO `mvcmusicstore`.`album` (`AlbumId`, `GenreId`, `ArtistId`, `Title`, `Price`, `AlbumArtUrl`) VALUES (444, 1, 22, 'Coda', 8.99, null);
INSERT INTO `mvcmusicstore`.`album` (`AlbumId`, `GenreId`, `ArtistId`, `Title`, `Price`, `AlbumArtUrl`) VALUES (445, 1, 22, 'Houses Of The Holy', 8.99, null);
INSERT INTO `mvcmusicstore`.`album` (`AlbumId`, `GenreId`, `ArtistId`, `Title`, `Price`, `AlbumArtUrl`) VALUES (446, 1, 22, 'In Through The Out Door', 8.99, null);
INSERT INTO `mvcmusicstore`.`album` (`AlbumId`, `GenreId`, `ArtistId`, `Title`, `Price`, `AlbumArtUrl`) VALUES (447, 1, 22, 'IV', 8.99, null);
INSERT INTO `mvcmusicstore`.`album` (`AlbumId`, `GenreId`, `ArtistId`, `Title`, `Price`, `AlbumArtUrl`) VALUES (448, 1, 22, 'Led Zeppelin I', 8.99, null);
INSERT INTO `mvcmusicstore`.`album` (`AlbumId`, `GenreId`, `ArtistId`, `Title`, `Price`, `AlbumArtUrl`) VALUES (449, 1, 22, 'Led Zeppelin II', 8.99, null);
INSERT INTO `mvcmusicstore`.`album` (`AlbumId`, `GenreId`, `ArtistId`, `Title`, `Price`, `AlbumArtUrl`) VALUES (450, 1, 22, 'Led Zeppelin III', 8.99, null);
INSERT INTO `mvcmusicstore`.`album` (`AlbumId`, `GenreId`, `ArtistId`, `Title`, `Price`, `AlbumArtUrl`) VALUES (451, 1, 22, 'Physical Graffiti [Disc 1', 8.99, null);
INSERT INTO `mvcmusicstore`.`album` (`AlbumId`, `GenreId`, `ArtistId`, `Title`, `Price`, `AlbumArtUrl`) VALUES (452, 1, 22, 'Physical Graffiti [Disc 2', 8.99, null);
INSERT INTO `mvcmusicstore`.`album` (`AlbumId`, `GenreId`, `ArtistId`, `Title`, `Price`, `AlbumArtUrl`) VALUES (453, 1, 22, 'Presence', 8.99, null);
INSERT INTO `mvcmusicstore`.`album` (`AlbumId`, `GenreId`, `ArtistId`, `Title`, `Price`, `AlbumArtUrl`) VALUES (454, 1, 22, 'The Song Remains The Same (Disc 1)', 8.99, null);
INSERT INTO `mvcmusicstore`.`album` (`AlbumId`, `GenreId`, `ArtistId`, `Title`, `Price`, `AlbumArtUrl`) VALUES (455, 1, 22, 'The Song Remains The Same (Disc 2)', 8.99, null);
INSERT INTO `mvcmusicstore`.`album` (`AlbumId`, `GenreId`, `ArtistId`, `Title`, `Price`, `AlbumArtUrl`) VALUES (456, 1, 23, 'Bongo Fury', 8.99, null);
INSERT INTO `mvcmusicstore`.`album` (`AlbumId`, `GenreId`, `ArtistId`, `Title`, `Price`, `AlbumArtUrl`) VALUES (457, 1, 3, 'Big Ones', 8.99, null);
INSERT INTO `mvcmusicstore`.`album` (`AlbumId`, `GenreId`, `ArtistId`, `Title`, `Price`, `AlbumArtUrl`) VALUES (458, 1, 4, 'Jagged Little Pill', 8.99, null);
INSERT INTO `mvcmusicstore`.`album` (`AlbumId`, `GenreId`, `ArtistId`, `Title`, `Price`, `AlbumArtUrl`) VALUES (459, 1, 5, 'Facelift', 8.99, null);
INSERT INTO `mvcmusicstore`.`album` (`AlbumId`, `GenreId`, `ArtistId`, `Title`, `Price`, `AlbumArtUrl`) VALUES (460, 1, 51, 'Greatest Hits I', 8.99, null);
INSERT INTO `mvcmusicstore`.`album` (`AlbumId`, `GenreId`, `ArtistId`, `Title`, `Price`, `AlbumArtUrl`) VALUES (461, 1, 51, 'Greatest Hits II', 8.99, null);
INSERT INTO `mvcmusicstore`.`album` (`AlbumId`, `GenreId`, `ArtistId`, `Title`, `Price`, `AlbumArtUrl`) VALUES (462, 1, 51, 'News Of The World', 8.99, null);
INSERT INTO `mvcmusicstore`.`album` (`AlbumId`, `GenreId`, `ArtistId`, `Title`, `Price`, `AlbumArtUrl`) VALUES (463, 1, 52, 'Greatest Kiss', 8.99, null);
INSERT INTO `mvcmusicstore`.`album` (`AlbumId`, `GenreId`, `ArtistId`, `Title`, `Price`, `AlbumArtUrl`) VALUES (464, 1, 52, 'Unplugged [Live', 8.99, null);
INSERT INTO `mvcmusicstore`.`album` (`AlbumId`, `GenreId`, `ArtistId`, `Title`, `Price`, `AlbumArtUrl`) VALUES (465, 1, 55, 'Into The Light', 8.99, null);
INSERT INTO `mvcmusicstore`.`album` (`AlbumId`, `GenreId`, `ArtistId`, `Title`, `Price`, `AlbumArtUrl`) VALUES (466, 1, 58, 'Come Taste The Band', 8.99, null);
INSERT INTO `mvcmusicstore`.`album` (`AlbumId`, `GenreId`, `ArtistId`, `Title`, `Price`, `AlbumArtUrl`) VALUES (467, 1, 58, 'Deep Purple In Rock', 8.99, null);
INSERT INTO `mvcmusicstore`.`album` (`AlbumId`, `GenreId`, `ArtistId`, `Title`, `Price`, `AlbumArtUrl`) VALUES (468, 1, 58, 'Fireball', 8.99, null);
INSERT INTO `mvcmusicstore`.`album` (`AlbumId`, `GenreId`, `ArtistId`, `Title`, `Price`, `AlbumArtUrl`) VALUES (469, 1, 58, 'Machine Head', 8.99, null);
INSERT INTO `mvcmusicstore`.`album` (`AlbumId`, `GenreId`, `ArtistId`, `Title`, `Price`, `AlbumArtUrl`) VALUES (470, 1, 58, 'MK III The Final Concerts [Disc 1', 8.99, null);
INSERT INTO `mvcmusicstore`.`album` (`AlbumId`, `GenreId`, `ArtistId`, `Title`, `Price`, `AlbumArtUrl`) VALUES (471, 1, 58, 'Purpendicular', 8.99, null);
INSERT INTO `mvcmusicstore`.`album` (`AlbumId`, `GenreId`, `ArtistId`, `Title`, `Price`, `AlbumArtUrl`) VALUES (472, 1, 58, 'Slaves And Masters', 8.99, null);
INSERT INTO `mvcmusicstore`.`album` (`AlbumId`, `GenreId`, `ArtistId`, `Title`, `Price`, `AlbumArtUrl`) VALUES (473, 1, 58, 'Stormbringer', 8.99, null);
INSERT INTO `mvcmusicstore`.`album` (`AlbumId`, `GenreId`, `ArtistId`, `Title`, `Price`, `AlbumArtUrl`) VALUES (474, 1, 58, 'The Battle Rages O', 8.99, null);
INSERT INTO `mvcmusicstore`.`album` (`AlbumId`, `GenreId`, `ArtistId`, `Title`, `Price`, `AlbumArtUrl`) VALUES (475, 1, 58, 'The Final Concerts (Disc 2)', 8.99, null);
INSERT INTO `mvcmusicstore`.`album` (`AlbumId`, `GenreId`, `ArtistId`, `Title`, `Price`, `AlbumArtUrl`) VALUES (476, 1, 59, 'Santana - As Years Go By', 8.99, null);
INSERT INTO `mvcmusicstore`.`album` (`AlbumId`, `GenreId`, `ArtistId`, `Title`, `Price`, `AlbumArtUrl`) VALUES (477, 1, 59, 'Santana Live', 8.99, null);
INSERT INTO `mvcmusicstore`.`album` (`AlbumId`, `GenreId`, `ArtistId`, `Title`, `Price`, `AlbumArtUrl`) VALUES (478, 1, 59, 'Supernatural', 8.99, null);
INSERT INTO `mvcmusicstore`.`album` (`AlbumId`, `GenreId`, `ArtistId`, `Title`, `Price`, `AlbumArtUrl`) VALUES (479, 1, 76, 'Chronicle, Vol. 1', 8.99, null);
INSERT INTO `mvcmusicstore`.`album` (`AlbumId`, `GenreId`, `ArtistId`, `Title`, `Price`, `AlbumArtUrl`) VALUES (480, 1, 76, 'Chronicle, Vol. 2', 8.99, null);
INSERT INTO `mvcmusicstore`.`album` (`AlbumId`, `GenreId`, `ArtistId`, `Title`, `Price`, `AlbumArtUrl`) VALUES (481, 1, 8, 'Audioslave', 8.99, null);
INSERT INTO `mvcmusicstore`.`album` (`AlbumId`, `GenreId`, `ArtistId`, `Title`, `Price`, `AlbumArtUrl`) VALUES (482, 1, 82, 'King For A Day Fool For A Lifetime', 8.99, null);
INSERT INTO `mvcmusicstore`.`album` (`AlbumId`, `GenreId`, `ArtistId`, `Title`, `Price`, `AlbumArtUrl`) VALUES (483, 1, 84, 'In Your Honor [Disc 1', 8.99, null);
INSERT INTO `mvcmusicstore`.`album` (`AlbumId`, `GenreId`, `ArtistId`, `Title`, `Price`, `AlbumArtUrl`) VALUES (484, 1, 84, 'In Your Honor [Disc 2', 8.99, null);
INSERT INTO `mvcmusicstore`.`album` (`AlbumId`, `GenreId`, `ArtistId`, `Title`, `Price`, `AlbumArtUrl`) VALUES (485, 1, 84, 'The Colour And The Shape', 8.99, null);
INSERT INTO `mvcmusicstore`.`album` (`AlbumId`, `GenreId`, `ArtistId`, `Title`, `Price`, `AlbumArtUrl`) VALUES (486, 1, 88, 'Appetite for Destructio', 8.99, null);
INSERT INTO `mvcmusicstore`.`album` (`AlbumId`, `GenreId`, `ArtistId`, `Title`, `Price`, `AlbumArtUrl`) VALUES (487, 1, 88, 'Use Your Illusion I', 8.99, null);
INSERT INTO `mvcmusicstore`.`album` (`AlbumId`, `GenreId`, `ArtistId`, `Title`, `Price`, `AlbumArtUrl`) VALUES (488, 1, 90, 'A Matter of Life and Death', 8.99, null);
INSERT INTO `mvcmusicstore`.`album` (`AlbumId`, `GenreId`, `ArtistId`, `Title`, `Price`, `AlbumArtUrl`) VALUES (489, 1, 90, 'Brave New World', 8.99, null);
INSERT INTO `mvcmusicstore`.`album` (`AlbumId`, `GenreId`, `ArtistId`, `Title`, `Price`, `AlbumArtUrl`) VALUES (490, 1, 90, 'Fear Of The Dark', 8.99, null);
INSERT INTO `mvcmusicstore`.`album` (`AlbumId`, `GenreId`, `ArtistId`, `Title`, `Price`, `AlbumArtUrl`) VALUES (491, 1, 90, 'Live At Donington 1992 (Disc 1)', 8.99, null);
INSERT INTO `mvcmusicstore`.`album` (`AlbumId`, `GenreId`, `ArtistId`, `Title`, `Price`, `AlbumArtUrl`) VALUES (492, 1, 90, 'Live At Donington 1992 (Disc 2)', 8.99, null);
INSERT INTO `mvcmusicstore`.`album` (`AlbumId`, `GenreId`, `ArtistId`, `Title`, `Price`, `AlbumArtUrl`) VALUES (493, 1, 90, 'Rock In Rio [CD2', 8.99, null);
INSERT INTO `mvcmusicstore`.`album` (`AlbumId`, `GenreId`, `ArtistId`, `Title`, `Price`, `AlbumArtUrl`) VALUES (494, 1, 90, 'The Number of The Beast', 8.99, null);
INSERT INTO `mvcmusicstore`.`album` (`AlbumId`, `GenreId`, `ArtistId`, `Title`, `Price`, `AlbumArtUrl`) VALUES (495, 1, 90, 'The X Factor', 8.99, null);
INSERT INTO `mvcmusicstore`.`album` (`AlbumId`, `GenreId`, `ArtistId`, `Title`, `Price`, `AlbumArtUrl`) VALUES (496, 1, 90, 'Virtual XI', 8.99, null);
INSERT INTO `mvcmusicstore`.`album` (`AlbumId`, `GenreId`, `ArtistId`, `Title`, `Price`, `AlbumArtUrl`) VALUES (497, 1, 92, 'Emergency On Planet Earth', 8.99, null);
INSERT INTO `mvcmusicstore`.`album` (`AlbumId`, `GenreId`, `ArtistId`, `Title`, `Price`, `AlbumArtUrl`) VALUES (498, 1, 94, 'Are You Experienced?', 8.99, null);
INSERT INTO `mvcmusicstore`.`album` (`AlbumId`, `GenreId`, `ArtistId`, `Title`, `Price`, `AlbumArtUrl`) VALUES (499, 1, 95, 'Surfing with the Alien (Remastered)', 8.99, null);
INSERT INTO `mvcmusicstore`.`album` (`AlbumId`, `GenreId`, `ArtistId`, `Title`, `Price`, `AlbumArtUrl`) VALUES (500, 10, 203, 'The Best of Beethove', 8.99, null);
INSERT INTO `mvcmusicstore`.`album` (`AlbumId`, `GenreId`, `ArtistId`, `Title`, `Price`, `AlbumArtUrl`) VALUES (504, 10, 208, 'Pachelbel: Canon & Gigue', 8.99, null);
INSERT INTO `mvcmusicstore`.`album` (`AlbumId`, `GenreId`, `ArtistId`, `Title`, `Price`, `AlbumArtUrl`) VALUES (507, 10, 211, 'Bach: Goldberg Variations', 8.99, null);
INSERT INTO `mvcmusicstore`.`album` (`AlbumId`, `GenreId`, `ArtistId`, `Title`, `Price`, `AlbumArtUrl`) VALUES (508, 10, 212, 'Bach: The Cello Suites', 8.99, null);
INSERT INTO `mvcmusicstore`.`album` (`AlbumId`, `GenreId`, `ArtistId`, `Title`, `Price`, `AlbumArtUrl`) VALUES (509, 10, 213, 'Handel: The Messiah (Highlights)', 8.99, null);
INSERT INTO `mvcmusicstore`.`album` (`AlbumId`, `GenreId`, `ArtistId`, `Title`, `Price`, `AlbumArtUrl`) VALUES (513, 10, 217, 'Haydn: Symphonies 99 - 104', 8.99, null);
INSERT INTO `mvcmusicstore`.`album` (`AlbumId`, `GenreId`, `ArtistId`, `Title`, `Price`, `AlbumArtUrl`) VALUES (515, 10, 219, 'A Soprano Inspired', 8.99, null);
INSERT INTO `mvcmusicstore`.`album` (`AlbumId`, `GenreId`, `ArtistId`, `Title`, `Price`, `AlbumArtUrl`) VALUES (517, 10, 221, 'Wagner: Favourite Overtures', 8.99, null);
INSERT INTO `mvcmusicstore`.`album` (`AlbumId`, `GenreId`, `ArtistId`, `Title`, `Price`, `AlbumArtUrl`) VALUES (519, 10, 223, 'Tchaikovsky: The Nutcracker', 8.99, null);
INSERT INTO `mvcmusicstore`.`album` (`AlbumId`, `GenreId`, `ArtistId`, `Title`, `Price`, `AlbumArtUrl`) VALUES (520, 10, 224, 'The Last Night of the Proms', 8.99, null);
INSERT INTO `mvcmusicstore`.`album` (`AlbumId`, `GenreId`, `ArtistId`, `Title`, `Price`, `AlbumArtUrl`) VALUES (523, 10, 226, 'Respighi:Pines of Rome', 8.99, null);
INSERT INTO `mvcmusicstore`.`album` (`AlbumId`, `GenreId`, `ArtistId`, `Title`, `Price`, `AlbumArtUrl`) VALUES (524, 10, 226, 'Strauss: Waltzes', 8.99, null);
INSERT INTO `mvcmusicstore`.`album` (`AlbumId`, `GenreId`, `ArtistId`, `Title`, `Price`, `AlbumArtUrl`) VALUES (525, 10, 229, 'Carmina Burana', 8.99, null);
INSERT INTO `mvcmusicstore`.`album` (`AlbumId`, `GenreId`, `ArtistId`, `Title`, `Price`, `AlbumArtUrl`) VALUES (526, 10, 230, 'A Copland Celebration, Vol. I', 8.99, null);
INSERT INTO `mvcmusicstore`.`album` (`AlbumId`, `GenreId`, `ArtistId`, `Title`, `Price`, `AlbumArtUrl`) VALUES (527, 10, 231, 'Bach: Toccata & Fugue in D Minor', 8.99, null);
INSERT INTO `mvcmusicstore`.`album` (`AlbumId`, `GenreId`, `ArtistId`, `Title`, `Price`, `AlbumArtUrl`) VALUES (528, 10, 232, 'Prokofiev: Symphony No.1', 8.99, null);
INSERT INTO `mvcmusicstore`.`album` (`AlbumId`, `GenreId`, `ArtistId`, `Title`, `Price`, `AlbumArtUrl`) VALUES (529, 10, 233, 'Scheherazade', 8.99, null);
INSERT INTO `mvcmusicstore`.`album` (`AlbumId`, `GenreId`, `ArtistId`, `Title`, `Price`, `AlbumArtUrl`) VALUES (530, 10, 234, 'Bach: The Brandenburg Concertos', 8.99, null);
INSERT INTO `mvcmusicstore`.`album` (`AlbumId`, `GenreId`, `ArtistId`, `Title`, `Price`, `AlbumArtUrl`) VALUES (532, 10, 236, 'Mascagni: Cavalleria Rusticana', 8.99, null);
INSERT INTO `mvcmusicstore`.`album` (`AlbumId`, `GenreId`, `ArtistId`, `Title`, `Price`, `AlbumArtUrl`) VALUES (533, 10, 237, 'Sibelius: Finlandia', 8.99, null);
INSERT INTO `mvcmusicstore`.`album` (`AlbumId`, `GenreId`, `ArtistId`, `Title`, `Price`, `AlbumArtUrl`) VALUES (537, 10, 242, 'Adams, John: The Chairman Dances', 8.99, null);
INSERT INTO `mvcmusicstore`.`album` (`AlbumId`, `GenreId`, `ArtistId`, `Title`, `Price`, `AlbumArtUrl`) VALUES (539, 10, 245, 'Berlioz: Symphonie Fantastique', 8.99, null);
INSERT INTO `mvcmusicstore`.`album` (`AlbumId`, `GenreId`, `ArtistId`, `Title`, `Price`, `AlbumArtUrl`) VALUES (540, 10, 245, 'Prokofiev: Romeo & Juliet', 8.99, null);
INSERT INTO `mvcmusicstore`.`album` (`AlbumId`, `GenreId`, `ArtistId`, `Title`, `Price`, `AlbumArtUrl`) VALUES (542, 10, 247, 'English Renaissance', 8.99, null);
INSERT INTO `mvcmusicstore`.`album` (`AlbumId`, `GenreId`, `ArtistId`, `Title`, `Price`, `AlbumArtUrl`) VALUES (544, 10, 248, 'Mozart: Symphonies Nos. 40 & 41', 8.99, null);
INSERT INTO `mvcmusicstore`.`album` (`AlbumId`, `GenreId`, `ArtistId`, `Title`, `Price`, `AlbumArtUrl`) VALUES (546, 10, 250, 'SCRIABIN: Vers la flamme', 8.99, null);
INSERT INTO `mvcmusicstore`.`album` (`AlbumId`, `GenreId`, `ArtistId`, `Title`, `Price`, `AlbumArtUrl`) VALUES (548, 10, 255, 'Bartok: Violin & Viola Concertos', 8.99, null);
INSERT INTO `mvcmusicstore`.`album` (`AlbumId`, `GenreId`, `ArtistId`, `Title`, `Price`, `AlbumArtUrl`) VALUES (551, 10, 259, 'South American Getaway', 8.99, null);
INSERT INTO `mvcmusicstore`.`album` (`AlbumId`, `GenreId`, `ArtistId`, `Title`, `Price`, `AlbumArtUrl`) VALUES (552, 10, 260, 'Górecki: Symphony No. 3', 8.99, null);
INSERT INTO `mvcmusicstore`.`album` (`AlbumId`, `GenreId`, `ArtistId`, `Title`, `Price`, `AlbumArtUrl`) VALUES (553, 10, 261, 'Purcell: The Fairy Quee', 8.99, null);
INSERT INTO `mvcmusicstore`.`album` (`AlbumId`, `GenreId`, `ArtistId`, `Title`, `Price`, `AlbumArtUrl`) VALUES (556, 10, 264, 'Weill: The Seven Deadly Sins', 8.99, null);
INSERT INTO `mvcmusicstore`.`album` (`AlbumId`, `GenreId`, `ArtistId`, `Title`, `Price`, `AlbumArtUrl`) VALUES (558, 10, 266, 'Szymanowski: Piano Works, Vol. 1', 8.99, null);
INSERT INTO `mvcmusicstore`.`album` (`AlbumId`, `GenreId`, `ArtistId`, `Title`, `Price`, `AlbumArtUrl`) VALUES (559, 10, 267, 'Nielsen: The Six Symphonies', 8.99, null);
INSERT INTO `mvcmusicstore`.`album` (`AlbumId`, `GenreId`, `ArtistId`, `Title`, `Price`, `AlbumArtUrl`) VALUES (562, 10, 274, 'Mozart: Chamber Music', 8.99, null);
INSERT INTO `mvcmusicstore`.`album` (`AlbumId`, `GenreId`, `ArtistId`, `Title`, `Price`, `AlbumArtUrl`) VALUES (563, 2, 10, 'The Best Of Billy Cobham', 8.99, null);
INSERT INTO `mvcmusicstore`.`album` (`AlbumId`, `GenreId`, `ArtistId`, `Title`, `Price`, `AlbumArtUrl`) VALUES (564, 2, 197, 'Quiet Songs', 8.99, null);
INSERT INTO `mvcmusicstore`.`album` (`AlbumId`, `GenreId`, `ArtistId`, `Title`, `Price`, `AlbumArtUrl`) VALUES (565, 2, 202, 'Worlds', 8.99, null);
INSERT INTO `mvcmusicstore`.`album` (`AlbumId`, `GenreId`, `ArtistId`, `Title`, `Price`, `AlbumArtUrl`) VALUES (566, 2, 27, 'Quanta Gente Veio ver--Bônus De Carnaval', 8.99, null);
INSERT INTO `mvcmusicstore`.`album` (`AlbumId`, `GenreId`, `ArtistId`, `Title`, `Price`, `AlbumArtUrl`) VALUES (567, 2, 53, 'Heart of the Night', 8.99, null);
INSERT INTO `mvcmusicstore`.`album` (`AlbumId`, `GenreId`, `ArtistId`, `Title`, `Price`, `AlbumArtUrl`) VALUES (568, 2, 53, 'Morning Dance', 8.99, null);
INSERT INTO `mvcmusicstore`.`album` (`AlbumId`, `GenreId`, `ArtistId`, `Title`, `Price`, `AlbumArtUrl`) VALUES (569, 2, 6, 'Warner 25 Anos', 8.99, null);
INSERT INTO `mvcmusicstore`.`album` (`AlbumId`, `GenreId`, `ArtistId`, `Title`, `Price`, `AlbumArtUrl`) VALUES (570, 2, 68, 'Miles Ahead', 8.99, null);
INSERT INTO `mvcmusicstore`.`album` (`AlbumId`, `GenreId`, `ArtistId`, `Title`, `Price`, `AlbumArtUrl`) VALUES (571, 2, 68, 'The Essential Miles Davis [Disc 1', 8.99, null);
INSERT INTO `mvcmusicstore`.`album` (`AlbumId`, `GenreId`, `ArtistId`, `Title`, `Price`, `AlbumArtUrl`) VALUES (572, 2, 68, 'The Essential Miles Davis [Disc 2', 8.99, null);
INSERT INTO `mvcmusicstore`.`album` (`AlbumId`, `GenreId`, `ArtistId`, `Title`, `Price`, `AlbumArtUrl`) VALUES (573, 2, 79, 'Outbreak', 8.99, null);
INSERT INTO `mvcmusicstore`.`album` (`AlbumId`, `GenreId`, `ArtistId`, `Title`, `Price`, `AlbumArtUrl`) VALUES (574, 2, 89, 'Blue Moods', 8.99, null);
INSERT INTO `mvcmusicstore`.`album` (`AlbumId`, `GenreId`, `ArtistId`, `Title`, `Price`, `AlbumArtUrl`) VALUES (575, 3, 100, 'Greatest Hits', 8.99, null);
INSERT INTO `mvcmusicstore`.`album` (`AlbumId`, `GenreId`, `ArtistId`, `Title`, `Price`, `AlbumArtUrl`) VALUES (576, 3, 106, 'Ace Of Spades', 8.99, null);
INSERT INTO `mvcmusicstore`.`album` (`AlbumId`, `GenreId`, `ArtistId`, `Title`, `Price`, `AlbumArtUrl`) VALUES (577, 3, 109, 'Motley Crue Greatest Hits', 8.99, null);
INSERT INTO `mvcmusicstore`.`album` (`AlbumId`, `GenreId`, `ArtistId`, `Title`, `Price`, `AlbumArtUrl`) VALUES (578, 3, 11, 'Alcohol Fueled Brewtality Live! [Disc 1', 8.99, null);
INSERT INTO `mvcmusicstore`.`album` (`AlbumId`, `GenreId`, `ArtistId`, `Title`, `Price`, `AlbumArtUrl`) VALUES (579, 3, 11, 'Alcohol Fueled Brewtality Live! [Disc 2', 8.99, null);
INSERT INTO `mvcmusicstore`.`album` (`AlbumId`, `GenreId`, `ArtistId`, `Title`, `Price`, `AlbumArtUrl`) VALUES (580, 3, 114, 'Tribute', 8.99, null);
INSERT INTO `mvcmusicstore`.`album` (`AlbumId`, `GenreId`, `ArtistId`, `Title`, `Price`, `AlbumArtUrl`) VALUES (581, 3, 12, 'Black Sabbath Vol. 4 (Remaster)', 8.99, null);
INSERT INTO `mvcmusicstore`.`album` (`AlbumId`, `GenreId`, `ArtistId`, `Title`, `Price`, `AlbumArtUrl`) VALUES (582, 3, 12, 'Black Sabbath', 8.99, null);
INSERT INTO `mvcmusicstore`.`album` (`AlbumId`, `GenreId`, `ArtistId`, `Title`, `Price`, `AlbumArtUrl`) VALUES (583, 3, 135, 'Mezmerize', 8.99, null);
INSERT INTO `mvcmusicstore`.`album` (`AlbumId`, `GenreId`, `ArtistId`, `Title`, `Price`, `AlbumArtUrl`) VALUES (584, 3, 14, 'Chemical Wedding', 8.99, null);
INSERT INTO `mvcmusicstore`.`album` (`AlbumId`, `GenreId`, `ArtistId`, `Title`, `Price`, `AlbumArtUrl`) VALUES (585, 3, 50, '...And Justice For All', 8.99, null);
INSERT INTO `mvcmusicstore`.`album` (`AlbumId`, `GenreId`, `ArtistId`, `Title`, `Price`, `AlbumArtUrl`) VALUES (586, 3, 50, 'Black Album', 8.99, null);
INSERT INTO `mvcmusicstore`.`album` (`AlbumId`, `GenreId`, `ArtistId`, `Title`, `Price`, `AlbumArtUrl`) VALUES (587, 3, 50, 'Garage Inc. (Disc 1)', 8.99, null);
INSERT INTO `mvcmusicstore`.`album` (`AlbumId`, `GenreId`, `ArtistId`, `Title`, `Price`, `AlbumArtUrl`) VALUES (588, 3, 50, 'Garage Inc. (Disc 2)', 8.99, null);
INSERT INTO `mvcmusicstore`.`album` (`AlbumId`, `GenreId`, `ArtistId`, `Title`, `Price`, `AlbumArtUrl`) VALUES (589, 3, 50, 'Load', 8.99, null);
INSERT INTO `mvcmusicstore`.`album` (`AlbumId`, `GenreId`, `ArtistId`, `Title`, `Price`, `AlbumArtUrl`) VALUES (590, 3, 50, 'Master Of Puppets', 8.99, null);
INSERT INTO `mvcmusicstore`.`album` (`AlbumId`, `GenreId`, `ArtistId`, `Title`, `Price`, `AlbumArtUrl`) VALUES (591, 3, 50, 'ReLoad', 8.99, null);
INSERT INTO `mvcmusicstore`.`album` (`AlbumId`, `GenreId`, `ArtistId`, `Title`, `Price`, `AlbumArtUrl`) VALUES (592, 3, 50, 'Ride The Lightning', 8.99, null);
INSERT INTO `mvcmusicstore`.`album` (`AlbumId`, `GenreId`, `ArtistId`, `Title`, `Price`, `AlbumArtUrl`) VALUES (593, 3, 50, 'St. Anger', 8.99, null);
INSERT INTO `mvcmusicstore`.`album` (`AlbumId`, `GenreId`, `ArtistId`, `Title`, `Price`, `AlbumArtUrl`) VALUES (594, 3, 7, 'Plays Metallica By Four Cellos', 8.99, null);
INSERT INTO `mvcmusicstore`.`album` (`AlbumId`, `GenreId`, `ArtistId`, `Title`, `Price`, `AlbumArtUrl`) VALUES (595, 3, 87, 'Faceless', 8.99, null);
INSERT INTO `mvcmusicstore`.`album` (`AlbumId`, `GenreId`, `ArtistId`, `Title`, `Price`, `AlbumArtUrl`) VALUES (596, 3, 88, 'Use Your Illusion II', 8.99, null);
INSERT INTO `mvcmusicstore`.`album` (`AlbumId`, `GenreId`, `ArtistId`, `Title`, `Price`, `AlbumArtUrl`) VALUES (597, 3, 90, 'A Real Dead One', 8.99, null);
INSERT INTO `mvcmusicstore`.`album` (`AlbumId`, `GenreId`, `ArtistId`, `Title`, `Price`, `AlbumArtUrl`) VALUES (598, 3, 90, 'A Real Live One', 8.99, null);
INSERT INTO `mvcmusicstore`.`album` (`AlbumId`, `GenreId`, `ArtistId`, `Title`, `Price`, `AlbumArtUrl`) VALUES (599, 3, 90, 'Live After Death', 8.99, null);
INSERT INTO `mvcmusicstore`.`album` (`AlbumId`, `GenreId`, `ArtistId`, `Title`, `Price`, `AlbumArtUrl`) VALUES (600, 3, 90, 'No Prayer For The Dying', 8.99, null);
INSERT INTO `mvcmusicstore`.`album` (`AlbumId`, `GenreId`, `ArtistId`, `Title`, `Price`, `AlbumArtUrl`) VALUES (601, 3, 90, 'Piece Of Mind', 8.99, null);
INSERT INTO `mvcmusicstore`.`album` (`AlbumId`, `GenreId`, `ArtistId`, `Title`, `Price`, `AlbumArtUrl`) VALUES (602, 3, 90, 'Powerslave', 8.99, null);
INSERT INTO `mvcmusicstore`.`album` (`AlbumId`, `GenreId`, `ArtistId`, `Title`, `Price`, `AlbumArtUrl`) VALUES (603, 3, 90, 'Rock In Rio [CD1', 8.99, null);
INSERT INTO `mvcmusicstore`.`album` (`AlbumId`, `GenreId`, `ArtistId`, `Title`, `Price`, `AlbumArtUrl`) VALUES (604, 3, 90, 'Rock In Rio [CD2', 8.99, null);
INSERT INTO `mvcmusicstore`.`album` (`AlbumId`, `GenreId`, `ArtistId`, `Title`, `Price`, `AlbumArtUrl`) VALUES (605, 3, 90, 'Seventh Son of a Seventh So', 8.99, null);
INSERT INTO `mvcmusicstore`.`album` (`AlbumId`, `GenreId`, `ArtistId`, `Title`, `Price`, `AlbumArtUrl`) VALUES (606, 3, 90, 'Somewhere in Time', 8.99, null);
INSERT INTO `mvcmusicstore`.`album` (`AlbumId`, `GenreId`, `ArtistId`, `Title`, `Price`, `AlbumArtUrl`) VALUES (607, 3, 90, 'The Number of The Beast', 8.99, null);
INSERT INTO `mvcmusicstore`.`album` (`AlbumId`, `GenreId`, `ArtistId`, `Title`, `Price`, `AlbumArtUrl`) VALUES (608, 3, 98, 'Living After Midnight', 8.99, null);
INSERT INTO `mvcmusicstore`.`album` (`AlbumId`, `GenreId`, `ArtistId`, `Title`, `Price`, `AlbumArtUrl`) VALUES (609, 4, 196, 'Cake: B-Sides and Rarities', 8.99, null);
INSERT INTO `mvcmusicstore`.`album` (`AlbumId`, `GenreId`, `ArtistId`, `Title`, `Price`, `AlbumArtUrl`) VALUES (610, 4, 204, 'Temple of the Dog', 8.99, null);
INSERT INTO `mvcmusicstore`.`album` (`AlbumId`, `GenreId`, `ArtistId`, `Title`, `Price`, `AlbumArtUrl`) VALUES (611, 4, 205, 'Carry O', 8.99, null);
INSERT INTO `mvcmusicstore`.`album` (`AlbumId`, `GenreId`, `ArtistId`, `Title`, `Price`, `AlbumArtUrl`) VALUES (612, 4, 253, 'Carried to Dust (Bonus Track Version)', 8.99, null);
INSERT INTO `mvcmusicstore`.`album` (`AlbumId`, `GenreId`, `ArtistId`, `Title`, `Price`, `AlbumArtUrl`) VALUES (613, 4, 8, 'Revelations', 8.99, null);
INSERT INTO `mvcmusicstore`.`album` (`AlbumId`, `GenreId`, `ArtistId`, `Title`, `Price`, `AlbumArtUrl`) VALUES (614, 6, 133, 'In Step', 8.99, null);
INSERT INTO `mvcmusicstore`.`album` (`AlbumId`, `GenreId`, `ArtistId`, `Title`, `Price`, `AlbumArtUrl`) VALUES (615, 6, 137, 'Live [Disc 1', 8.99, null);
INSERT INTO `mvcmusicstore`.`album` (`AlbumId`, `GenreId`, `ArtistId`, `Title`, `Price`, `AlbumArtUrl`) VALUES (616, 6, 137, 'Live [Disc 2', 8.99, null);
INSERT INTO `mvcmusicstore`.`album` (`AlbumId`, `GenreId`, `ArtistId`, `Title`, `Price`, `AlbumArtUrl`) VALUES (618, 6, 81, 'The Cream Of Clapto', 8.99, null);
INSERT INTO `mvcmusicstore`.`album` (`AlbumId`, `GenreId`, `ArtistId`, `Title`, `Price`, `AlbumArtUrl`) VALUES (619, 6, 81, 'Unplugged', 8.99, null);
INSERT INTO `mvcmusicstore`.`album` (`AlbumId`, `GenreId`, `ArtistId`, `Title`, `Price`, `AlbumArtUrl`) VALUES (620, 6, 90, 'Iron Maide', 8.99, null);
INSERT INTO `mvcmusicstore`.`album` (`AlbumId`, `GenreId`, `ArtistId`, `Title`, `Price`, `AlbumArtUrl`) VALUES (623, 7, 103, 'Barulhinho Bom', 8.99, null);
INSERT INTO `mvcmusicstore`.`album` (`AlbumId`, `GenreId`, `ArtistId`, `Title`, `Price`, `AlbumArtUrl`) VALUES (624, 7, 112, 'Olodum', 8.99, null);
INSERT INTO `mvcmusicstore`.`album` (`AlbumId`, `GenreId`, `ArtistId`, `Title`, `Price`, `AlbumArtUrl`) VALUES (625, 7, 113, 'Acústico MTV', 8.99, null);
INSERT INTO `mvcmusicstore`.`album` (`AlbumId`, `GenreId`, `ArtistId`, `Title`, `Price`, `AlbumArtUrl`) VALUES (626, 7, 113, 'Arquivo II', 8.99, null);
INSERT INTO `mvcmusicstore`.`album` (`AlbumId`, `GenreId`, `ArtistId`, `Title`, `Price`, `AlbumArtUrl`) VALUES (627, 7, 113, 'Arquivo Os Paralamas Do Sucesso', 8.99, null);
INSERT INTO `mvcmusicstore`.`album` (`AlbumId`, `GenreId`, `ArtistId`, `Title`, `Price`, `AlbumArtUrl`) VALUES (628, 7, 145, 'Serie Sem Limite (Disc 1)', 8.99, null);
INSERT INTO `mvcmusicstore`.`album` (`AlbumId`, `GenreId`, `ArtistId`, `Title`, `Price`, `AlbumArtUrl`) VALUES (629, 7, 145, 'Serie Sem Limite (Disc 2)', 8.99, null);
INSERT INTO `mvcmusicstore`.`album` (`AlbumId`, `GenreId`, `ArtistId`, `Title`, `Price`, `AlbumArtUrl`) VALUES (630, 7, 155, 'Ao Vivo [IMPORT', 8.99, null);
INSERT INTO `mvcmusicstore`.`album` (`AlbumId`, `GenreId`, `ArtistId`, `Title`, `Price`, `AlbumArtUrl`) VALUES (631, 7, 16, 'Prenda Minha', 8.99, null);
INSERT INTO `mvcmusicstore`.`album` (`AlbumId`, `GenreId`, `ArtistId`, `Title`, `Price`, `AlbumArtUrl`) VALUES (632, 7, 16, 'Sozinho Remix Ao Vivo', 8.99, null);
INSERT INTO `mvcmusicstore`.`album` (`AlbumId`, `GenreId`, `ArtistId`, `Title`, `Price`, `AlbumArtUrl`) VALUES (633, 7, 17, 'Minha Historia', 8.99, null);
INSERT INTO `mvcmusicstore`.`album` (`AlbumId`, `GenreId`, `ArtistId`, `Title`, `Price`, `AlbumArtUrl`) VALUES (634, 7, 18, 'Afrociberdelia', 8.99, null);
INSERT INTO `mvcmusicstore`.`album` (`AlbumId`, `GenreId`, `ArtistId`, `Title`, `Price`, `AlbumArtUrl`) VALUES (635, 7, 18, 'Da Lama Ao Caos', 8.99, null);
INSERT INTO `mvcmusicstore`.`album` (`AlbumId`, `GenreId`, `ArtistId`, `Title`, `Price`, `AlbumArtUrl`) VALUES (636, 7, 20, 'Na Pista', 8.99, null);
INSERT INTO `mvcmusicstore`.`album` (`AlbumId`, `GenreId`, `ArtistId`, `Title`, `Price`, `AlbumArtUrl`) VALUES (637, 7, 201, 'Duos II', 8.99, null);
INSERT INTO `mvcmusicstore`.`album` (`AlbumId`, `GenreId`, `ArtistId`, `Title`, `Price`, `AlbumArtUrl`) VALUES (638, 7, 21, 'Sambas De Enredo 2001', 8.99, null);
INSERT INTO `mvcmusicstore`.`album` (`AlbumId`, `GenreId`, `ArtistId`, `Title`, `Price`, `AlbumArtUrl`) VALUES (639, 7, 21, 'Vozes do MPB', 8.99, null);
INSERT INTO `mvcmusicstore`.`album` (`AlbumId`, `GenreId`, `ArtistId`, `Title`, `Price`, `AlbumArtUrl`) VALUES (640, 7, 24, 'Chill: Brazil (Disc 1)', 8.99, null);
INSERT INTO `mvcmusicstore`.`album` (`AlbumId`, `GenreId`, `ArtistId`, `Title`, `Price`, `AlbumArtUrl`) VALUES (641, 7, 27, 'Quanta Gente Veio Ver (Live)', 8.99, null);
INSERT INTO `mvcmusicstore`.`album` (`AlbumId`, `GenreId`, `ArtistId`, `Title`, `Price`, `AlbumArtUrl`) VALUES (642, 7, 37, 'The Best of Ed Motta', 8.99, null);
INSERT INTO `mvcmusicstore`.`album` (`AlbumId`, `GenreId`, `ArtistId`, `Title`, `Price`, `AlbumArtUrl`) VALUES (643, 7, 41, 'Elis Regina-Minha História', 8.99, null);
INSERT INTO `mvcmusicstore`.`album` (`AlbumId`, `GenreId`, `ArtistId`, `Title`, `Price`, `AlbumArtUrl`) VALUES (644, 7, 42, 'Milton Nascimento Ao Vivo', 8.99, null);
INSERT INTO `mvcmusicstore`.`album` (`AlbumId`, `GenreId`, `ArtistId`, `Title`, `Price`, `AlbumArtUrl`) VALUES (645, 7, 42, 'Minas', 8.99, null);
INSERT INTO `mvcmusicstore`.`album` (`AlbumId`, `GenreId`, `ArtistId`, `Title`, `Price`, `AlbumArtUrl`) VALUES (646, 7, 46, 'Jorge Ben Jor 25 Anos', 8.99, null);
INSERT INTO `mvcmusicstore`.`album` (`AlbumId`, `GenreId`, `ArtistId`, `Title`, `Price`, `AlbumArtUrl`) VALUES (647, 7, 56, 'Meus Momentos', 8.99, null);
INSERT INTO `mvcmusicstore`.`album` (`AlbumId`, `GenreId`, `ArtistId`, `Title`, `Price`, `AlbumArtUrl`) VALUES (648, 7, 6, 'Chill: Brazil (Disc 2)', 8.99, null);
INSERT INTO `mvcmusicstore`.`album` (`AlbumId`, `GenreId`, `ArtistId`, `Title`, `Price`, `AlbumArtUrl`) VALUES (649, 7, 72, 'Vinicius De Moraes', 8.99, null);
INSERT INTO `mvcmusicstore`.`album` (`AlbumId`, `GenreId`, `ArtistId`, `Title`, `Price`, `AlbumArtUrl`) VALUES (651, 7, 77, 'Cássia Eller - Sem Limite [Disc 1', 8.99, null);
INSERT INTO `mvcmusicstore`.`album` (`AlbumId`, `GenreId`, `ArtistId`, `Title`, `Price`, `AlbumArtUrl`) VALUES (652, 7, 80, 'Djavan Ao Vivo - Vol. 02', 8.99, null);
INSERT INTO `mvcmusicstore`.`album` (`AlbumId`, `GenreId`, `ArtistId`, `Title`, `Price`, `AlbumArtUrl`) VALUES (653, 7, 80, 'Djavan Ao Vivo - Vol. 1', 8.99, null);
INSERT INTO `mvcmusicstore`.`album` (`AlbumId`, `GenreId`, `ArtistId`, `Title`, `Price`, `AlbumArtUrl`) VALUES (654, 7, 81, 'Unplugged', 8.99, null);
INSERT INTO `mvcmusicstore`.`album` (`AlbumId`, `GenreId`, `ArtistId`, `Title`, `Price`, `AlbumArtUrl`) VALUES (655, 7, 83, 'Deixa Entrar', 8.99, null);
INSERT INTO `mvcmusicstore`.`album` (`AlbumId`, `GenreId`, `ArtistId`, `Title`, `Price`, `AlbumArtUrl`) VALUES (656, 7, 86, 'Roda De Funk', 8.99, null);
INSERT INTO `mvcmusicstore`.`album` (`AlbumId`, `GenreId`, `ArtistId`, `Title`, `Price`, `AlbumArtUrl`) VALUES (657, 7, 96, 'Jota Quest-1995', 8.99, null);
INSERT INTO `mvcmusicstore`.`album` (`AlbumId`, `GenreId`, `ArtistId`, `Title`, `Price`, `AlbumArtUrl`) VALUES (659, 7, 99, 'Mais Do Mesmo', 8.99, null);
INSERT INTO `mvcmusicstore`.`album` (`AlbumId`, `GenreId`, `ArtistId`, `Title`, `Price`, `AlbumArtUrl`) VALUES (660, 8, 100, 'Greatest Hits', 8.99, null);
INSERT INTO `mvcmusicstore`.`album` (`AlbumId`, `GenreId`, `ArtistId`, `Title`, `Price`, `AlbumArtUrl`) VALUES (661, 8, 151, 'UB40 The Best Of - Volume Two [UK', 8.99, null);
INSERT INTO `mvcmusicstore`.`album` (`AlbumId`, `GenreId`, `ArtistId`, `Title`, `Price`, `AlbumArtUrl`) VALUES (662, 8, 19, 'Acústico MTV [Live', 8.99, null);
INSERT INTO `mvcmusicstore`.`album` (`AlbumId`, `GenreId`, `ArtistId`, `Title`, `Price`, `AlbumArtUrl`) VALUES (663, 8, 19, 'Cidade Negra - Hits', 8.99, null);
INSERT INTO `mvcmusicstore`.`album` (`AlbumId`, `GenreId`, `ArtistId`, `Title`, `Price`, `AlbumArtUrl`) VALUES (665, 9, 21, 'Axé Bahia 2001', 8.99, null);
INSERT INTO `mvcmusicstore`.`album` (`AlbumId`, `GenreId`, `ArtistId`, `Title`, `Price`, `AlbumArtUrl`) VALUES (666, 9, 252, 'Frank', 8.99, null);
INSERT INTO `mvcmusicstore`.`album` (`AlbumId`, `GenreId`, `ArtistId`, `Title`, `Price`, `AlbumArtUrl`) VALUES (667, 5, 276, 'Le Freak', 8.99, null);
INSERT INTO `mvcmusicstore`.`album` (`AlbumId`, `GenreId`, `ArtistId`, `Title`, `Price`, `AlbumArtUrl`) VALUES (668, 5, 278, 'MacArthur Park Suite', 8.99, null);
INSERT INTO `mvcmusicstore`.`album` (`AlbumId`, `GenreId`, `ArtistId`, `Title`, `Price`, `AlbumArtUrl`) VALUES (669, 5, 277, 'Ring My Bell', 8.99, null);
INSERT INTO `mvcmusicstore`.`album` (`AlbumId`, `GenreId`, `ArtistId`, `Title`, `Price`, `AlbumArtUrl`) VALUES (670, 5, 276, 'I have no idea ', 8.99, null);
INSERT INTO `mvcmusicstore`.`album` (`AlbumId`, `GenreId`, `ArtistId`, `Title`, `Price`, `AlbumArtUrl`) VALUES (671, 2, 2, 'Really', 7.00, 'Simple man.jfif');
INSERT INTO `mvcmusicstore`.`album` (`AlbumId`, `GenreId`, `ArtistId`, `Title`, `Price`, `AlbumArtUrl`) VALUES (672, 1, 42, 'Hot red spy', 9.00, 'Oh my god.jfif');
INSERT INTO `mvcmusicstore`.`album` (`AlbumId`, `GenreId`, `ArtistId`, `Title`, `Price`, `AlbumArtUrl`) VALUES (673, 6, 21, 'Totally spy', 11.00, 'Nas-gods-son-music-album.jpg');
INSERT INTO `mvcmusicstore`.`album` (`AlbumId`, `GenreId`, `ArtistId`, `Title`, `Price`, `AlbumArtUrl`) VALUES (675, 7, 20, 'Haha Land', 9.00, 'Michael-Buble--To-Be-Loved-album-cover.jpg');
INSERT INTO `mvcmusicstore`.`album` (`AlbumId`, `GenreId`, `ArtistId`, `Title`, `Price`, `AlbumArtUrl`) VALUES (676, 2, 2, 'Now what', 8.00, 'the-boxer-rebellion-ocean-by-ocean-album-cover-full-size.jpg');
INSERT INTO `mvcmusicstore`.`album` (`AlbumId`, `GenreId`, `ArtistId`, `Title`, `Price`, `AlbumArtUrl`) VALUES (677, 3, 141, 'Hey shut up', 9.00, 'Simple man.jfif');
INSERT INTO `mvcmusicstore`.`album` (`AlbumId`, `GenreId`, `ArtistId`, `Title`, `Price`, `AlbumArtUrl`) VALUES (679, 9, 4, 'aaaaaaa', 3.00, 'meghan-trainor-thank-you-album-cover-2016-426x426.jpg');

COMMIT;


-- -----------------------------------------------------
-- Data for table `mvcmusicstore`.`user`
-- -----------------------------------------------------
START TRANSACTION;
USE `mvcmusicstore`;
INSERT INTO `mvcmusicstore`.`user` (`UserId`, `Username`, `Password`, `FirstName`, `LastName`, `Address`, `City`, `State`, `PostalCode`, `Country`, `Phone`, `Email`, `Status`, `Type`) VALUES (UserId, 'Username', 'Password', 'FirstName', 'LastName', 'Address', 'City', 'State', 'PostalCode', 'Country', 'Phone', 'Email', 'Status', 'Type');
INSERT INTO `mvcmusicstore`.`user` (`UserId`, `Username`, `Password`, `FirstName`, `LastName`, `Address`, `City`, `State`, `PostalCode`, `Country`, `Phone`, `Email`, `Status`, `Type`) VALUES (1, 'bill', 'abc123', 'Bill', 'Watcher', 'NULL', 'NULL', 'NULL', 'NULL', 'NULL', 'NULL', 'bill@xyz.com', 'Active', 'USER');
INSERT INTO `mvcmusicstore`.`user` (`UserId`, `Username`, `Password`, `FirstName`, `LastName`, `Address`, `City`, `State`, `PostalCode`, `Country`, `Phone`, `Email`, `Status`, `Type`) VALUES (2, 'danny', 'abc123', 'Danny', 'Theys', 'NULL', 'NULL', 'NULL', 'NULL', 'NULL', 'NULL', 'danny@xyz.com', 'Active', 'USER');
INSERT INTO `mvcmusicstore`.`user` (`UserId`, `Username`, `Password`, `FirstName`, `LastName`, `Address`, `City`, `State`, `PostalCode`, `Country`, `Phone`, `Email`, `Status`, `Type`) VALUES (3, 'sam', 'abc125', 'Sam', 'Smith', 'NULL', 'NULL', 'NULL', 'NULL', 'NULL', 'NULL', 'samy@xyz.com', 'Active', 'ADMIN');
INSERT INTO `mvcmusicstore`.`user` (`UserId`, `Username`, `Password`, `FirstName`, `LastName`, `Address`, `City`, `State`, `PostalCode`, `Country`, `Phone`, `Email`, `Status`, `Type`) VALUES (4, 'nicole', 'abc126', 'Nicole', 'warner', 'NULL', 'NULL', 'NULL', 'NULL', 'NULL', 'NULL', 'nicloe@xyz.com', 'Active', 'USER');
INSERT INTO `mvcmusicstore`.`user` (`UserId`, `Username`, `Password`, `FirstName`, `LastName`, `Address`, `City`, `State`, `PostalCode`, `Country`, `Phone`, `Email`, `Status`, `Type`) VALUES (5, 'kenny', 'abc127', 'Kenny', 'Roger', 'NULL', 'NULL', 'NULL', 'NULL', 'NULL', 'NULL', 'kenny@xyz.com', 'Active', 'USER');
INSERT INTO `mvcmusicstore`.`user` (`UserId`, `Username`, `Password`, `FirstName`, `LastName`, `Address`, `City`, `State`, `PostalCode`, `Country`, `Phone`, `Email`, `Status`, `Type`) VALUES (10, 'a', '$2a$10$k59lS9oWSxPjZg3vn/PiKuFv28ywbEhZuXkOG2M4hUxG8tK/nmHlW', 'a', 'a', 'a', 'a', 'a', 'a', 'a', '1', 'a', 'Active', 'ADMIN');

COMMIT;

